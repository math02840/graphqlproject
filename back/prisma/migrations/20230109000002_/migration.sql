/*
  Warnings:

  - You are about to drop the column `commentId` on the `Comment` table. All the data in the column will be lost.
  - You are about to drop the column `ownerId` on the `Comment` table. All the data in the column will be lost.
  - You are about to drop the column `targetType` on the `Comment` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[parent_id]` on the table `Comment` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `userId` to the `Comment` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "Comment" DROP CONSTRAINT "Comment_commentId_fkey";

-- DropForeignKey
ALTER TABLE "Comment" DROP CONSTRAINT "Comment_ownerId_fkey";

-- AlterTable
ALTER TABLE "Comment" DROP COLUMN "commentId",
DROP COLUMN "ownerId",
DROP COLUMN "targetType",
ADD COLUMN     "parent_id" TEXT,
ADD COLUMN     "target_type" "TargetType",
ADD COLUMN     "userId" TEXT NOT NULL,
ALTER COLUMN "created_at" DROP NOT NULL,
ALTER COLUMN "created_at" DROP DEFAULT,
ALTER COLUMN "updated_at" DROP NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "Comment_parent_id_key" ON "Comment"("parent_id");

-- AddForeignKey
ALTER TABLE "Comment" ADD CONSTRAINT "Comment_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
