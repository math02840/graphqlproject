import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../scalars";
import { Action } from "../models/Action";
import { User } from "../models/User";

@TypeGraphQL.ObjectType("Notification", {
  isAbstract: true,
  simpleResolvers: true
})
export class Notification {
  @TypeGraphQL.Field(_type => TypeGraphQL.ID, {
    nullable: false
  })
  id!: string;

  user_id?: User | null;

  action_id?: Action | null;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  readed_at?: Date | null;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  userId?: string | null;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  actionId?: string | null;
}
