import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../scalars";
import { Notification } from "../models/Notification";
import { Task } from "../models/Task";
import { ActionGroup } from "../enums/ActionGroup";
import { ActionName } from "../enums/ActionName";
import { TargetType } from "../enums/TargetType";
import { ActionCount } from "../resolvers/outputs/ActionCount";

@TypeGraphQL.ObjectType("Action", {
  isAbstract: true,
  simpleResolvers: true
})
export class Action {
  @TypeGraphQL.Field(_type => TypeGraphQL.ID, {
    nullable: false
  })
  id!: string;

  @TypeGraphQL.Field(_type => ActionGroup, {
    nullable: false
  })
  group!: "TASK" | "COMMENT";

  @TypeGraphQL.Field(_type => ActionName, {
    nullable: false
  })
  name!: "CREATE" | "ADD" | "UPDATE" | "DELETE" | "COMMENT" | "ASSIGN" | "UNASSIGN";

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  created_at?: Date | null;

  target_id?: Task | null;

  @TypeGraphQL.Field(_type => TargetType, {
    nullable: false
  })
  target_type!: "FEATURE" | "BUG";

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  taskId?: string | null;

  notifications?: Notification[];

  @TypeGraphQL.Field(_type => ActionCount, {
    nullable: true
  })
  _count?: ActionCount | null;
}
