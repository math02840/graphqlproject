import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../scalars";
import { Task } from "../models/Task";
import { User } from "../models/User";

@TypeGraphQL.ObjectType("Assignee", {
  isAbstract: true,
  simpleResolvers: true
})
export class Assignee {
  @TypeGraphQL.Field(_type => TypeGraphQL.ID, {
    nullable: false
  })
  id!: string;

  task_id?: Task | null;

  user?: User | null;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  created_at?: Date | null;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  taskId?: string | null;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  userId?: string | null;
}
