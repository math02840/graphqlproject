import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../scalars";
import { Task } from "../models/Task";
import { User } from "../models/User";
import { TargetType } from "../enums/TargetType";

@TypeGraphQL.ObjectType("Comment", {
  isAbstract: true,
  simpleResolvers: true
})
export class Comment {
  @TypeGraphQL.Field(_type => TypeGraphQL.ID, {
    nullable: false
  })
  id!: string;

  owner_id?: User | null;

  target_id?: Task | null;

  @TypeGraphQL.Field(_type => TargetType, {
    nullable: true
  })
  target_type?: "FEATURE" | "BUG" | null;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  parent_id?: string | null;

  @TypeGraphQL.Field(_type => String, {
    nullable: false
  })
  content!: string;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  created_at?: Date | null;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  updated_at?: Date | null;

  @TypeGraphQL.Field(_type => String, {
    nullable: false
  })
  userId!: string;

  @TypeGraphQL.Field(_type => String, {
    nullable: false
  })
  taskId!: string;
}
