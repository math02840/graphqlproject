import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../scalars";
import { Action } from "../models/Action";
import { Assignee } from "../models/Assignee";
import { Comment } from "../models/Comment";
import { User } from "../models/User";
import { TaskState } from "../enums/TaskState";
import { TaskCount } from "../resolvers/outputs/TaskCount";

@TypeGraphQL.ObjectType("Task", {
  isAbstract: true,
  simpleResolvers: true
})
export class Task {
  @TypeGraphQL.Field(_type => TypeGraphQL.ID, {
    nullable: false
  })
  id!: string;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  title?: string | null;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  description?: string | null;

  owner_id?: User | null;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  due_at?: Date | null;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  created_at?: Date | null;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  updated_at?: Date | null;

  @TypeGraphQL.Field(_type => TaskState, {
    nullable: false
  })
  state!: "TODO" | "IN_PROGRESS" | "DONE";

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  userId?: string | null;

  comment?: Comment[];

  assignees?: Assignee[];

  action?: Action[];

  @TypeGraphQL.Field(_type => TaskCount, {
    nullable: true
  })
  _count?: TaskCount | null;
}
