import * as TypeGraphQL from "type-graphql";

export enum TaskState {
  TODO = "TODO",
  IN_PROGRESS = "IN_PROGRESS",
  DONE = "DONE"
}
TypeGraphQL.registerEnumType(TaskState, {
  name: "TaskState",
  description: undefined,
});
