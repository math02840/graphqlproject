import * as TypeGraphQL from "type-graphql";

export enum TargetType {
  FEATURE = "FEATURE",
  BUG = "BUG"
}
TypeGraphQL.registerEnumType(TargetType, {
  name: "TargetType",
  description: undefined,
});
