import * as TypeGraphQL from "type-graphql";

export enum CommentScalarFieldEnum {
  id = "id",
  target_type = "target_type",
  parent_id = "parent_id",
  content = "content",
  created_at = "created_at",
  updated_at = "updated_at",
  userId = "userId",
  taskId = "taskId"
}
TypeGraphQL.registerEnumType(CommentScalarFieldEnum, {
  name: "CommentScalarFieldEnum",
  description: undefined,
});
