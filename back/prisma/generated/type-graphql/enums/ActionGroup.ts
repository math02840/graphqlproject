import * as TypeGraphQL from "type-graphql";

export enum ActionGroup {
  TASK = "TASK",
  COMMENT = "COMMENT"
}
TypeGraphQL.registerEnumType(ActionGroup, {
  name: "ActionGroup",
  description: undefined,
});
