import * as TypeGraphQL from "type-graphql";

export enum AssigneeScalarFieldEnum {
  id = "id",
  created_at = "created_at",
  taskId = "taskId",
  userId = "userId"
}
TypeGraphQL.registerEnumType(AssigneeScalarFieldEnum, {
  name: "AssigneeScalarFieldEnum",
  description: undefined,
});
