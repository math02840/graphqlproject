import * as TypeGraphQL from "type-graphql";

export enum ActionName {
  CREATE = "CREATE",
  ADD = "ADD",
  UPDATE = "UPDATE",
  DELETE = "DELETE",
  COMMENT = "COMMENT",
  ASSIGN = "ASSIGN",
  UNASSIGN = "UNASSIGN"
}
TypeGraphQL.registerEnumType(ActionName, {
  name: "ActionName",
  description: undefined,
});
