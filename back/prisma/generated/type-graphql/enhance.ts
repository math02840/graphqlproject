import { ClassType } from "type-graphql";
import * as tslib from "tslib";
import * as crudResolvers from "./resolvers/crud/resolvers-crud.index";
import * as argsTypes from "./resolvers/crud/args.index";
import * as actionResolvers from "./resolvers/crud/resolvers-actions.index";
import * as relationResolvers from "./resolvers/relations/resolvers.index";
import * as models from "./models";
import * as outputTypes from "./resolvers/outputs";
import * as inputTypes from "./resolvers/inputs";

const crudResolversMap = {
  User: crudResolvers.UserCrudResolver,
  Task: crudResolvers.TaskCrudResolver,
  Comment: crudResolvers.CommentCrudResolver,
  Assignee: crudResolvers.AssigneeCrudResolver,
  Action: crudResolvers.ActionCrudResolver,
  Notification: crudResolvers.NotificationCrudResolver
};
const actionResolversMap = {
  User: {
    aggregateUser: actionResolvers.AggregateUserResolver,
    createManyUser: actionResolvers.CreateManyUserResolver,
    createOneUser: actionResolvers.CreateOneUserResolver,
    deleteManyUser: actionResolvers.DeleteManyUserResolver,
    deleteOneUser: actionResolvers.DeleteOneUserResolver,
    findFirstUser: actionResolvers.FindFirstUserResolver,
    findFirstUserOrThrow: actionResolvers.FindFirstUserOrThrowResolver,
    users: actionResolvers.FindManyUserResolver,
    user: actionResolvers.FindUniqueUserResolver,
    getUser: actionResolvers.FindUniqueUserOrThrowResolver,
    groupByUser: actionResolvers.GroupByUserResolver,
    updateManyUser: actionResolvers.UpdateManyUserResolver,
    updateOneUser: actionResolvers.UpdateOneUserResolver,
    upsertOneUser: actionResolvers.UpsertOneUserResolver
  },
  Task: {
    aggregateTask: actionResolvers.AggregateTaskResolver,
    createManyTask: actionResolvers.CreateManyTaskResolver,
    createOneTask: actionResolvers.CreateOneTaskResolver,
    deleteManyTask: actionResolvers.DeleteManyTaskResolver,
    deleteOneTask: actionResolvers.DeleteOneTaskResolver,
    findFirstTask: actionResolvers.FindFirstTaskResolver,
    findFirstTaskOrThrow: actionResolvers.FindFirstTaskOrThrowResolver,
    tasks: actionResolvers.FindManyTaskResolver,
    task: actionResolvers.FindUniqueTaskResolver,
    getTask: actionResolvers.FindUniqueTaskOrThrowResolver,
    groupByTask: actionResolvers.GroupByTaskResolver,
    updateManyTask: actionResolvers.UpdateManyTaskResolver,
    updateOneTask: actionResolvers.UpdateOneTaskResolver,
    upsertOneTask: actionResolvers.UpsertOneTaskResolver
  },
  Comment: {
    aggregateComment: actionResolvers.AggregateCommentResolver,
    createManyComment: actionResolvers.CreateManyCommentResolver,
    createOneComment: actionResolvers.CreateOneCommentResolver,
    deleteManyComment: actionResolvers.DeleteManyCommentResolver,
    deleteOneComment: actionResolvers.DeleteOneCommentResolver,
    findFirstComment: actionResolvers.FindFirstCommentResolver,
    findFirstCommentOrThrow: actionResolvers.FindFirstCommentOrThrowResolver,
    comments: actionResolvers.FindManyCommentResolver,
    comment: actionResolvers.FindUniqueCommentResolver,
    getComment: actionResolvers.FindUniqueCommentOrThrowResolver,
    groupByComment: actionResolvers.GroupByCommentResolver,
    updateManyComment: actionResolvers.UpdateManyCommentResolver,
    updateOneComment: actionResolvers.UpdateOneCommentResolver,
    upsertOneComment: actionResolvers.UpsertOneCommentResolver
  },
  Assignee: {
    aggregateAssignee: actionResolvers.AggregateAssigneeResolver,
    createManyAssignee: actionResolvers.CreateManyAssigneeResolver,
    createOneAssignee: actionResolvers.CreateOneAssigneeResolver,
    deleteManyAssignee: actionResolvers.DeleteManyAssigneeResolver,
    deleteOneAssignee: actionResolvers.DeleteOneAssigneeResolver,
    findFirstAssignee: actionResolvers.FindFirstAssigneeResolver,
    findFirstAssigneeOrThrow: actionResolvers.FindFirstAssigneeOrThrowResolver,
    assignees: actionResolvers.FindManyAssigneeResolver,
    assignee: actionResolvers.FindUniqueAssigneeResolver,
    getAssignee: actionResolvers.FindUniqueAssigneeOrThrowResolver,
    groupByAssignee: actionResolvers.GroupByAssigneeResolver,
    updateManyAssignee: actionResolvers.UpdateManyAssigneeResolver,
    updateOneAssignee: actionResolvers.UpdateOneAssigneeResolver,
    upsertOneAssignee: actionResolvers.UpsertOneAssigneeResolver
  },
  Action: {
    aggregateAction: actionResolvers.AggregateActionResolver,
    createManyAction: actionResolvers.CreateManyActionResolver,
    createOneAction: actionResolvers.CreateOneActionResolver,
    deleteManyAction: actionResolvers.DeleteManyActionResolver,
    deleteOneAction: actionResolvers.DeleteOneActionResolver,
    findFirstAction: actionResolvers.FindFirstActionResolver,
    findFirstActionOrThrow: actionResolvers.FindFirstActionOrThrowResolver,
    actions: actionResolvers.FindManyActionResolver,
    action: actionResolvers.FindUniqueActionResolver,
    getAction: actionResolvers.FindUniqueActionOrThrowResolver,
    groupByAction: actionResolvers.GroupByActionResolver,
    updateManyAction: actionResolvers.UpdateManyActionResolver,
    updateOneAction: actionResolvers.UpdateOneActionResolver,
    upsertOneAction: actionResolvers.UpsertOneActionResolver
  },
  Notification: {
    aggregateNotification: actionResolvers.AggregateNotificationResolver,
    createManyNotification: actionResolvers.CreateManyNotificationResolver,
    createOneNotification: actionResolvers.CreateOneNotificationResolver,
    deleteManyNotification: actionResolvers.DeleteManyNotificationResolver,
    deleteOneNotification: actionResolvers.DeleteOneNotificationResolver,
    findFirstNotification: actionResolvers.FindFirstNotificationResolver,
    findFirstNotificationOrThrow: actionResolvers.FindFirstNotificationOrThrowResolver,
    notifications: actionResolvers.FindManyNotificationResolver,
    notification: actionResolvers.FindUniqueNotificationResolver,
    getNotification: actionResolvers.FindUniqueNotificationOrThrowResolver,
    groupByNotification: actionResolvers.GroupByNotificationResolver,
    updateManyNotification: actionResolvers.UpdateManyNotificationResolver,
    updateOneNotification: actionResolvers.UpdateOneNotificationResolver,
    upsertOneNotification: actionResolvers.UpsertOneNotificationResolver
  }
};
const crudResolversInfo = {
  User: ["aggregateUser", "createManyUser", "createOneUser", "deleteManyUser", "deleteOneUser", "findFirstUser", "findFirstUserOrThrow", "users", "user", "getUser", "groupByUser", "updateManyUser", "updateOneUser", "upsertOneUser"],
  Task: ["aggregateTask", "createManyTask", "createOneTask", "deleteManyTask", "deleteOneTask", "findFirstTask", "findFirstTaskOrThrow", "tasks", "task", "getTask", "groupByTask", "updateManyTask", "updateOneTask", "upsertOneTask"],
  Comment: ["aggregateComment", "createManyComment", "createOneComment", "deleteManyComment", "deleteOneComment", "findFirstComment", "findFirstCommentOrThrow", "comments", "comment", "getComment", "groupByComment", "updateManyComment", "updateOneComment", "upsertOneComment"],
  Assignee: ["aggregateAssignee", "createManyAssignee", "createOneAssignee", "deleteManyAssignee", "deleteOneAssignee", "findFirstAssignee", "findFirstAssigneeOrThrow", "assignees", "assignee", "getAssignee", "groupByAssignee", "updateManyAssignee", "updateOneAssignee", "upsertOneAssignee"],
  Action: ["aggregateAction", "createManyAction", "createOneAction", "deleteManyAction", "deleteOneAction", "findFirstAction", "findFirstActionOrThrow", "actions", "action", "getAction", "groupByAction", "updateManyAction", "updateOneAction", "upsertOneAction"],
  Notification: ["aggregateNotification", "createManyNotification", "createOneNotification", "deleteManyNotification", "deleteOneNotification", "findFirstNotification", "findFirstNotificationOrThrow", "notifications", "notification", "getNotification", "groupByNotification", "updateManyNotification", "updateOneNotification", "upsertOneNotification"]
};
const argsInfo = {
  AggregateUserArgs: ["where", "orderBy", "cursor", "take", "skip"],
  CreateManyUserArgs: ["data", "skipDuplicates"],
  CreateOneUserArgs: ["data"],
  DeleteManyUserArgs: ["where"],
  DeleteOneUserArgs: ["where"],
  FindFirstUserArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindFirstUserOrThrowArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindManyUserArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindUniqueUserArgs: ["where"],
  FindUniqueUserOrThrowArgs: ["where"],
  GroupByUserArgs: ["where", "orderBy", "by", "having", "take", "skip"],
  UpdateManyUserArgs: ["data", "where"],
  UpdateOneUserArgs: ["data", "where"],
  UpsertOneUserArgs: ["where", "create", "update"],
  AggregateTaskArgs: ["where", "orderBy", "cursor", "take", "skip"],
  CreateManyTaskArgs: ["data", "skipDuplicates"],
  CreateOneTaskArgs: ["data"],
  DeleteManyTaskArgs: ["where"],
  DeleteOneTaskArgs: ["where"],
  FindFirstTaskArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindFirstTaskOrThrowArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindManyTaskArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindUniqueTaskArgs: ["where"],
  FindUniqueTaskOrThrowArgs: ["where"],
  GroupByTaskArgs: ["where", "orderBy", "by", "having", "take", "skip"],
  UpdateManyTaskArgs: ["data", "where"],
  UpdateOneTaskArgs: ["data", "where"],
  UpsertOneTaskArgs: ["where", "create", "update"],
  AggregateCommentArgs: ["where", "orderBy", "cursor", "take", "skip"],
  CreateManyCommentArgs: ["data", "skipDuplicates"],
  CreateOneCommentArgs: ["data"],
  DeleteManyCommentArgs: ["where"],
  DeleteOneCommentArgs: ["where"],
  FindFirstCommentArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindFirstCommentOrThrowArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindManyCommentArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindUniqueCommentArgs: ["where"],
  FindUniqueCommentOrThrowArgs: ["where"],
  GroupByCommentArgs: ["where", "orderBy", "by", "having", "take", "skip"],
  UpdateManyCommentArgs: ["data", "where"],
  UpdateOneCommentArgs: ["data", "where"],
  UpsertOneCommentArgs: ["where", "create", "update"],
  AggregateAssigneeArgs: ["where", "orderBy", "cursor", "take", "skip"],
  CreateManyAssigneeArgs: ["data", "skipDuplicates"],
  CreateOneAssigneeArgs: ["data"],
  DeleteManyAssigneeArgs: ["where"],
  DeleteOneAssigneeArgs: ["where"],
  FindFirstAssigneeArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindFirstAssigneeOrThrowArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindManyAssigneeArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindUniqueAssigneeArgs: ["where"],
  FindUniqueAssigneeOrThrowArgs: ["where"],
  GroupByAssigneeArgs: ["where", "orderBy", "by", "having", "take", "skip"],
  UpdateManyAssigneeArgs: ["data", "where"],
  UpdateOneAssigneeArgs: ["data", "where"],
  UpsertOneAssigneeArgs: ["where", "create", "update"],
  AggregateActionArgs: ["where", "orderBy", "cursor", "take", "skip"],
  CreateManyActionArgs: ["data", "skipDuplicates"],
  CreateOneActionArgs: ["data"],
  DeleteManyActionArgs: ["where"],
  DeleteOneActionArgs: ["where"],
  FindFirstActionArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindFirstActionOrThrowArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindManyActionArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindUniqueActionArgs: ["where"],
  FindUniqueActionOrThrowArgs: ["where"],
  GroupByActionArgs: ["where", "orderBy", "by", "having", "take", "skip"],
  UpdateManyActionArgs: ["data", "where"],
  UpdateOneActionArgs: ["data", "where"],
  UpsertOneActionArgs: ["where", "create", "update"],
  AggregateNotificationArgs: ["where", "orderBy", "cursor", "take", "skip"],
  CreateManyNotificationArgs: ["data", "skipDuplicates"],
  CreateOneNotificationArgs: ["data"],
  DeleteManyNotificationArgs: ["where"],
  DeleteOneNotificationArgs: ["where"],
  FindFirstNotificationArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindFirstNotificationOrThrowArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindManyNotificationArgs: ["where", "orderBy", "cursor", "take", "skip", "distinct"],
  FindUniqueNotificationArgs: ["where"],
  FindUniqueNotificationOrThrowArgs: ["where"],
  GroupByNotificationArgs: ["where", "orderBy", "by", "having", "take", "skip"],
  UpdateManyNotificationArgs: ["data", "where"],
  UpdateOneNotificationArgs: ["data", "where"],
  UpsertOneNotificationArgs: ["where", "create", "update"]
};

type ResolverModelNames = keyof typeof crudResolversMap;

type ModelResolverActionNames<
  TModel extends ResolverModelNames
> = keyof typeof crudResolversMap[TModel]["prototype"];

export type ResolverActionsConfig<
  TModel extends ResolverModelNames
> = Partial<Record<ModelResolverActionNames<TModel> | "_all", MethodDecorator[]>>;

export type ResolversEnhanceMap = {
  [TModel in ResolverModelNames]?: ResolverActionsConfig<TModel>;
};

export function applyResolversEnhanceMap(
  resolversEnhanceMap: ResolversEnhanceMap,
) {
  for (const resolversEnhanceMapKey of Object.keys(resolversEnhanceMap)) {
    const modelName = resolversEnhanceMapKey as keyof typeof resolversEnhanceMap;
    const crudTarget = crudResolversMap[modelName].prototype;
    const resolverActionsConfig = resolversEnhanceMap[modelName]!;
    const actionResolversConfig = actionResolversMap[modelName];
    if (resolverActionsConfig._all) {
      const allActionsDecorators = resolverActionsConfig._all;
      const resolverActionNames = crudResolversInfo[modelName as keyof typeof crudResolversInfo];
      for (const resolverActionName of resolverActionNames) {
        const actionTarget = (actionResolversConfig[
          resolverActionName as keyof typeof actionResolversConfig
        ] as Function).prototype;
        tslib.__decorate(allActionsDecorators, crudTarget, resolverActionName, null);
        tslib.__decorate(allActionsDecorators, actionTarget, resolverActionName, null);
      }
    }
    const resolverActionsToApply = Object.keys(resolverActionsConfig).filter(
      it => it !== "_all"
    );
    for (const resolverActionName of resolverActionsToApply) {
      const decorators = resolverActionsConfig[
        resolverActionName as keyof typeof resolverActionsConfig
      ] as MethodDecorator[];
      const actionTarget = (actionResolversConfig[
        resolverActionName as keyof typeof actionResolversConfig
      ] as Function).prototype;
      tslib.__decorate(decorators, crudTarget, resolverActionName, null);
      tslib.__decorate(decorators, actionTarget, resolverActionName, null);
    }
  }
}

type ArgsTypesNames = keyof typeof argsTypes;

type ArgFieldNames<TArgsType extends ArgsTypesNames> = Exclude<
  keyof typeof argsTypes[TArgsType]["prototype"],
  number | symbol
>;

type ArgFieldsConfig<
  TArgsType extends ArgsTypesNames
> = FieldsConfig<ArgFieldNames<TArgsType>>;

export type ArgConfig<TArgsType extends ArgsTypesNames> = {
  class?: ClassDecorator[];
  fields?: ArgFieldsConfig<TArgsType>;
};

export type ArgsTypesEnhanceMap = {
  [TArgsType in ArgsTypesNames]?: ArgConfig<TArgsType>;
};

export function applyArgsTypesEnhanceMap(
  argsTypesEnhanceMap: ArgsTypesEnhanceMap,
) {
  for (const argsTypesEnhanceMapKey of Object.keys(argsTypesEnhanceMap)) {
    const argsTypeName = argsTypesEnhanceMapKey as keyof typeof argsTypesEnhanceMap;
    const typeConfig = argsTypesEnhanceMap[argsTypeName]!;
    const typeClass = argsTypes[argsTypeName];
    const typeTarget = typeClass.prototype;
    applyTypeClassEnhanceConfig(
      typeConfig,
      typeClass,
      typeTarget,
      argsInfo[argsTypeName as keyof typeof argsInfo],
    );
  }
}

const relationResolversMap = {
  User: relationResolvers.UserRelationsResolver,
  Task: relationResolvers.TaskRelationsResolver,
  Comment: relationResolvers.CommentRelationsResolver,
  Assignee: relationResolvers.AssigneeRelationsResolver,
  Action: relationResolvers.ActionRelationsResolver,
  Notification: relationResolvers.NotificationRelationsResolver
};
const relationResolversInfo = {
  User: ["task", "comment", "assignees", "notification"],
  Task: ["owner_id", "comment", "assignees", "action"],
  Comment: ["owner_id", "target_id"],
  Assignee: ["task_id", "user"],
  Action: ["target_id", "notifications"],
  Notification: ["user_id", "action_id"]
};

type RelationResolverModelNames = keyof typeof relationResolversMap;

type RelationResolverActionNames<
  TModel extends RelationResolverModelNames
> = keyof typeof relationResolversMap[TModel]["prototype"];

export type RelationResolverActionsConfig<TModel extends RelationResolverModelNames>
  = Partial<Record<RelationResolverActionNames<TModel> | "_all", MethodDecorator[]>>;

export type RelationResolversEnhanceMap = {
  [TModel in RelationResolverModelNames]?: RelationResolverActionsConfig<TModel>;
};

export function applyRelationResolversEnhanceMap(
  relationResolversEnhanceMap: RelationResolversEnhanceMap,
) {
  for (const relationResolversEnhanceMapKey of Object.keys(relationResolversEnhanceMap)) {
    const modelName = relationResolversEnhanceMapKey as keyof typeof relationResolversEnhanceMap;
    const relationResolverTarget = relationResolversMap[modelName].prototype;
    const relationResolverActionsConfig = relationResolversEnhanceMap[modelName]!;
    if (relationResolverActionsConfig._all) {
      const allActionsDecorators = relationResolverActionsConfig._all;
      const relationResolverActionNames = relationResolversInfo[modelName as keyof typeof relationResolversInfo];
      for (const relationResolverActionName of relationResolverActionNames) {
        tslib.__decorate(allActionsDecorators, relationResolverTarget, relationResolverActionName, null);
      }
    }
    const relationResolverActionsToApply = Object.keys(relationResolverActionsConfig).filter(
      it => it !== "_all"
    );
    for (const relationResolverActionName of relationResolverActionsToApply) {
      const decorators = relationResolverActionsConfig[
        relationResolverActionName as keyof typeof relationResolverActionsConfig
      ] as MethodDecorator[];
      tslib.__decorate(decorators, relationResolverTarget, relationResolverActionName, null);
    }
  }
}

type TypeConfig = {
  class?: ClassDecorator[];
  fields?: FieldsConfig;
};

type FieldsConfig<TTypeKeys extends string = string> = Partial<
  Record<TTypeKeys | "_all", PropertyDecorator[]>
>;

function applyTypeClassEnhanceConfig<
  TEnhanceConfig extends TypeConfig,
  TType extends object
>(
  enhanceConfig: TEnhanceConfig,
  typeClass: ClassType<TType>,
  typePrototype: TType,
  typeFieldNames: string[]
) {
  if (enhanceConfig.class) {
    tslib.__decorate(enhanceConfig.class, typeClass);
  }
  if (enhanceConfig.fields) {
    if (enhanceConfig.fields._all) {
      const allFieldsDecorators = enhanceConfig.fields._all;
      for (const typeFieldName of typeFieldNames) {
        tslib.__decorate(allFieldsDecorators, typePrototype, typeFieldName, void 0);
      }
    }
    const configFieldsToApply = Object.keys(enhanceConfig.fields).filter(
      it => it !== "_all"
    );
    for (const typeFieldName of configFieldsToApply) {
      const fieldDecorators = enhanceConfig.fields[typeFieldName]!;
      tslib.__decorate(fieldDecorators, typePrototype, typeFieldName, void 0);
    }
  }
}

const modelsInfo = {
  User: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at"],
  Task: ["id", "title", "description", "due_at", "created_at", "updated_at", "state", "userId"],
  Comment: ["id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId"],
  Assignee: ["id", "created_at", "taskId", "userId"],
  Action: ["id", "group", "name", "created_at", "target_type", "taskId"],
  Notification: ["id", "readed_at", "userId", "actionId"]
};

type ModelNames = keyof typeof models;

type ModelFieldNames<TModel extends ModelNames> = Exclude<
  keyof typeof models[TModel]["prototype"],
  number | symbol
>;

type ModelFieldsConfig<TModel extends ModelNames> = FieldsConfig<
  ModelFieldNames<TModel>
>;

export type ModelConfig<TModel extends ModelNames> = {
  class?: ClassDecorator[];
  fields?: ModelFieldsConfig<TModel>;
};

export type ModelsEnhanceMap = {
  [TModel in ModelNames]?: ModelConfig<TModel>;
};

export function applyModelsEnhanceMap(modelsEnhanceMap: ModelsEnhanceMap) {
  for (const modelsEnhanceMapKey of Object.keys(modelsEnhanceMap)) {
    const modelName = modelsEnhanceMapKey as keyof typeof modelsEnhanceMap;
    const modelConfig = modelsEnhanceMap[modelName]!;
    const modelClass = models[modelName];
    const modelTarget = modelClass.prototype;
    applyTypeClassEnhanceConfig(
      modelConfig,
      modelClass,
      modelTarget,
      modelsInfo[modelName as keyof typeof modelsInfo],
    );
  }
}

const outputsInfo = {
  AggregateUser: ["_count", "_min", "_max"],
  UserGroupBy: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "_count", "_min", "_max"],
  AggregateTask: ["_count", "_min", "_max"],
  TaskGroupBy: ["id", "title", "description", "due_at", "created_at", "updated_at", "state", "userId", "_count", "_min", "_max"],
  AggregateComment: ["_count", "_min", "_max"],
  CommentGroupBy: ["id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId", "_count", "_min", "_max"],
  AggregateAssignee: ["_count", "_min", "_max"],
  AssigneeGroupBy: ["id", "created_at", "taskId", "userId", "_count", "_min", "_max"],
  AggregateAction: ["_count", "_min", "_max"],
  ActionGroupBy: ["id", "group", "name", "created_at", "target_type", "taskId", "_count", "_min", "_max"],
  AggregateNotification: ["_count", "_min", "_max"],
  NotificationGroupBy: ["id", "readed_at", "userId", "actionId", "_count", "_min", "_max"],
  AffectedRowsOutput: ["count"],
  UserCount: ["task", "comment", "assignees", "notification"],
  UserCountAggregate: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "_all"],
  UserMinAggregate: ["id", "name", "password_digest", "active", "email", "last_sign_in_at", "created_at", "updated_at"],
  UserMaxAggregate: ["id", "name", "password_digest", "active", "email", "last_sign_in_at", "created_at", "updated_at"],
  TaskCount: ["comment", "assignees", "action"],
  TaskCountAggregate: ["id", "title", "description", "due_at", "created_at", "updated_at", "state", "userId", "_all"],
  TaskMinAggregate: ["id", "title", "description", "due_at", "created_at", "updated_at", "state", "userId"],
  TaskMaxAggregate: ["id", "title", "description", "due_at", "created_at", "updated_at", "state", "userId"],
  CommentCountAggregate: ["id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId", "_all"],
  CommentMinAggregate: ["id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId"],
  CommentMaxAggregate: ["id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId"],
  AssigneeCountAggregate: ["id", "created_at", "taskId", "userId", "_all"],
  AssigneeMinAggregate: ["id", "created_at", "taskId", "userId"],
  AssigneeMaxAggregate: ["id", "created_at", "taskId", "userId"],
  ActionCount: ["notifications"],
  ActionCountAggregate: ["id", "group", "name", "created_at", "target_type", "taskId", "_all"],
  ActionMinAggregate: ["id", "group", "name", "created_at", "target_type", "taskId"],
  ActionMaxAggregate: ["id", "group", "name", "created_at", "target_type", "taskId"],
  NotificationCountAggregate: ["id", "readed_at", "userId", "actionId", "_all"],
  NotificationMinAggregate: ["id", "readed_at", "userId", "actionId"],
  NotificationMaxAggregate: ["id", "readed_at", "userId", "actionId"]
};

type OutputTypesNames = keyof typeof outputTypes;

type OutputTypeFieldNames<TOutput extends OutputTypesNames> = Exclude<
  keyof typeof outputTypes[TOutput]["prototype"],
  number | symbol
>;

type OutputTypeFieldsConfig<
  TOutput extends OutputTypesNames
> = FieldsConfig<OutputTypeFieldNames<TOutput>>;

export type OutputTypeConfig<TOutput extends OutputTypesNames> = {
  class?: ClassDecorator[];
  fields?: OutputTypeFieldsConfig<TOutput>;
};

export type OutputTypesEnhanceMap = {
  [TOutput in OutputTypesNames]?: OutputTypeConfig<TOutput>;
};

export function applyOutputTypesEnhanceMap(
  outputTypesEnhanceMap: OutputTypesEnhanceMap,
) {
  for (const outputTypeEnhanceMapKey of Object.keys(outputTypesEnhanceMap)) {
    const outputTypeName = outputTypeEnhanceMapKey as keyof typeof outputTypesEnhanceMap;
    const typeConfig = outputTypesEnhanceMap[outputTypeName]!;
    const typeClass = outputTypes[outputTypeName];
    const typeTarget = typeClass.prototype;
    applyTypeClassEnhanceConfig(
      typeConfig,
      typeClass,
      typeTarget,
      outputsInfo[outputTypeName as keyof typeof outputsInfo],
    );
  }
}

const inputsInfo = {
  UserWhereInput: ["AND", "OR", "NOT", "id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "task", "comment", "assignees", "notification"],
  UserOrderByWithRelationInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "task", "comment", "assignees", "notification"],
  UserWhereUniqueInput: ["id", "email"],
  UserOrderByWithAggregationInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "_count", "_max", "_min"],
  UserScalarWhereWithAggregatesInput: ["AND", "OR", "NOT", "id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at"],
  TaskWhereInput: ["AND", "OR", "NOT", "id", "title", "description", "owner_id", "due_at", "created_at", "updated_at", "state", "userId", "comment", "assignees", "action"],
  TaskOrderByWithRelationInput: ["id", "title", "description", "owner_id", "due_at", "created_at", "updated_at", "state", "userId", "comment", "assignees", "action"],
  TaskWhereUniqueInput: ["id"],
  TaskOrderByWithAggregationInput: ["id", "title", "description", "due_at", "created_at", "updated_at", "state", "userId", "_count", "_max", "_min"],
  TaskScalarWhereWithAggregatesInput: ["AND", "OR", "NOT", "id", "title", "description", "due_at", "created_at", "updated_at", "state", "userId"],
  CommentWhereInput: ["AND", "OR", "NOT", "id", "owner_id", "target_id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId"],
  CommentOrderByWithRelationInput: ["id", "owner_id", "target_id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId"],
  CommentWhereUniqueInput: ["id", "parent_id"],
  CommentOrderByWithAggregationInput: ["id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId", "_count", "_max", "_min"],
  CommentScalarWhereWithAggregatesInput: ["AND", "OR", "NOT", "id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId"],
  AssigneeWhereInput: ["AND", "OR", "NOT", "id", "task_id", "user", "created_at", "taskId", "userId"],
  AssigneeOrderByWithRelationInput: ["id", "task_id", "user", "created_at", "taskId", "userId"],
  AssigneeWhereUniqueInput: ["id"],
  AssigneeOrderByWithAggregationInput: ["id", "created_at", "taskId", "userId", "_count", "_max", "_min"],
  AssigneeScalarWhereWithAggregatesInput: ["AND", "OR", "NOT", "id", "created_at", "taskId", "userId"],
  ActionWhereInput: ["AND", "OR", "NOT", "id", "group", "name", "created_at", "target_id", "target_type", "taskId", "notifications"],
  ActionOrderByWithRelationInput: ["id", "group", "name", "created_at", "target_id", "target_type", "taskId", "notifications"],
  ActionWhereUniqueInput: ["id"],
  ActionOrderByWithAggregationInput: ["id", "group", "name", "created_at", "target_type", "taskId", "_count", "_max", "_min"],
  ActionScalarWhereWithAggregatesInput: ["AND", "OR", "NOT", "id", "group", "name", "created_at", "target_type", "taskId"],
  NotificationWhereInput: ["AND", "OR", "NOT", "id", "user_id", "action_id", "readed_at", "userId", "actionId"],
  NotificationOrderByWithRelationInput: ["id", "user_id", "action_id", "readed_at", "userId", "actionId"],
  NotificationWhereUniqueInput: ["id"],
  NotificationOrderByWithAggregationInput: ["id", "readed_at", "userId", "actionId", "_count", "_max", "_min"],
  NotificationScalarWhereWithAggregatesInput: ["AND", "OR", "NOT", "id", "readed_at", "userId", "actionId"],
  UserCreateInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "task", "comment", "assignees", "notification"],
  UserUpdateInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "task", "comment", "assignees", "notification"],
  UserCreateManyInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at"],
  UserUpdateManyMutationInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at"],
  TaskCreateInput: ["id", "title", "description", "owner_id", "due_at", "created_at", "updated_at", "state", "comment", "assignees", "action"],
  TaskUpdateInput: ["id", "title", "description", "owner_id", "due_at", "created_at", "updated_at", "state", "comment", "assignees", "action"],
  TaskCreateManyInput: ["id", "title", "description", "due_at", "created_at", "updated_at", "state", "userId"],
  TaskUpdateManyMutationInput: ["id", "title", "description", "due_at", "created_at", "updated_at", "state"],
  CommentCreateInput: ["id", "owner_id", "target_id", "target_type", "parent_id", "content", "created_at", "updated_at"],
  CommentUpdateInput: ["id", "owner_id", "target_id", "target_type", "parent_id", "content", "created_at", "updated_at"],
  CommentCreateManyInput: ["id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId"],
  CommentUpdateManyMutationInput: ["id", "target_type", "parent_id", "content", "created_at", "updated_at"],
  AssigneeCreateInput: ["id", "task_id", "user", "created_at"],
  AssigneeUpdateInput: ["id", "task_id", "user", "created_at"],
  AssigneeCreateManyInput: ["id", "created_at", "taskId", "userId"],
  AssigneeUpdateManyMutationInput: ["id", "created_at"],
  ActionCreateInput: ["id", "group", "name", "created_at", "target_id", "target_type", "notifications"],
  ActionUpdateInput: ["id", "group", "name", "created_at", "target_id", "target_type", "notifications"],
  ActionCreateManyInput: ["id", "group", "name", "created_at", "target_type", "taskId"],
  ActionUpdateManyMutationInput: ["id", "group", "name", "created_at", "target_type"],
  NotificationCreateInput: ["id", "user_id", "action_id", "readed_at"],
  NotificationUpdateInput: ["id", "user_id", "action_id", "readed_at"],
  NotificationCreateManyInput: ["id", "readed_at", "userId", "actionId"],
  NotificationUpdateManyMutationInput: ["id", "readed_at"],
  StringFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "contains", "startsWith", "endsWith", "mode", "not"],
  StringNullableFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "contains", "startsWith", "endsWith", "mode", "not"],
  BoolNullableFilter: ["equals", "not"],
  JsonNullableFilter: ["equals", "path", "string_contains", "string_starts_with", "string_ends_with", "array_contains", "array_starts_with", "array_ends_with", "lt", "lte", "gt", "gte", "not"],
  DateTimeNullableFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "not"],
  TaskListRelationFilter: ["every", "some", "none"],
  CommentListRelationFilter: ["every", "some", "none"],
  AssigneeListRelationFilter: ["every", "some", "none"],
  NotificationListRelationFilter: ["every", "some", "none"],
  TaskOrderByRelationAggregateInput: ["_count"],
  CommentOrderByRelationAggregateInput: ["_count"],
  AssigneeOrderByRelationAggregateInput: ["_count"],
  NotificationOrderByRelationAggregateInput: ["_count"],
  UserCountOrderByAggregateInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at"],
  UserMaxOrderByAggregateInput: ["id", "name", "password_digest", "active", "email", "last_sign_in_at", "created_at", "updated_at"],
  UserMinOrderByAggregateInput: ["id", "name", "password_digest", "active", "email", "last_sign_in_at", "created_at", "updated_at"],
  StringWithAggregatesFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "contains", "startsWith", "endsWith", "mode", "not", "_count", "_min", "_max"],
  StringNullableWithAggregatesFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "contains", "startsWith", "endsWith", "mode", "not", "_count", "_min", "_max"],
  BoolNullableWithAggregatesFilter: ["equals", "not", "_count", "_min", "_max"],
  JsonNullableWithAggregatesFilter: ["equals", "path", "string_contains", "string_starts_with", "string_ends_with", "array_contains", "array_starts_with", "array_ends_with", "lt", "lte", "gt", "gte", "not", "_count", "_min", "_max"],
  DateTimeNullableWithAggregatesFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "not", "_count", "_min", "_max"],
  UserRelationFilter: ["is", "isNot"],
  EnumTaskStateFilter: ["equals", "in", "notIn", "not"],
  ActionListRelationFilter: ["every", "some", "none"],
  ActionOrderByRelationAggregateInput: ["_count"],
  TaskCountOrderByAggregateInput: ["id", "title", "description", "due_at", "created_at", "updated_at", "state", "userId"],
  TaskMaxOrderByAggregateInput: ["id", "title", "description", "due_at", "created_at", "updated_at", "state", "userId"],
  TaskMinOrderByAggregateInput: ["id", "title", "description", "due_at", "created_at", "updated_at", "state", "userId"],
  EnumTaskStateWithAggregatesFilter: ["equals", "in", "notIn", "not", "_count", "_min", "_max"],
  TaskRelationFilter: ["is", "isNot"],
  EnumTargetTypeNullableFilter: ["equals", "in", "notIn", "not"],
  CommentCountOrderByAggregateInput: ["id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId"],
  CommentMaxOrderByAggregateInput: ["id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId"],
  CommentMinOrderByAggregateInput: ["id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId"],
  EnumTargetTypeNullableWithAggregatesFilter: ["equals", "in", "notIn", "not", "_count", "_min", "_max"],
  AssigneeCountOrderByAggregateInput: ["id", "created_at", "taskId", "userId"],
  AssigneeMaxOrderByAggregateInput: ["id", "created_at", "taskId", "userId"],
  AssigneeMinOrderByAggregateInput: ["id", "created_at", "taskId", "userId"],
  EnumActionGroupFilter: ["equals", "in", "notIn", "not"],
  EnumActionNameFilter: ["equals", "in", "notIn", "not"],
  EnumTargetTypeFilter: ["equals", "in", "notIn", "not"],
  ActionCountOrderByAggregateInput: ["id", "group", "name", "created_at", "target_type", "taskId"],
  ActionMaxOrderByAggregateInput: ["id", "group", "name", "created_at", "target_type", "taskId"],
  ActionMinOrderByAggregateInput: ["id", "group", "name", "created_at", "target_type", "taskId"],
  EnumActionGroupWithAggregatesFilter: ["equals", "in", "notIn", "not", "_count", "_min", "_max"],
  EnumActionNameWithAggregatesFilter: ["equals", "in", "notIn", "not", "_count", "_min", "_max"],
  EnumTargetTypeWithAggregatesFilter: ["equals", "in", "notIn", "not", "_count", "_min", "_max"],
  ActionRelationFilter: ["is", "isNot"],
  NotificationCountOrderByAggregateInput: ["id", "readed_at", "userId", "actionId"],
  NotificationMaxOrderByAggregateInput: ["id", "readed_at", "userId", "actionId"],
  NotificationMinOrderByAggregateInput: ["id", "readed_at", "userId", "actionId"],
  TaskCreateNestedManyWithoutOwner_idInput: ["create", "connectOrCreate", "createMany", "connect"],
  CommentCreateNestedManyWithoutOwner_idInput: ["create", "connectOrCreate", "createMany", "connect"],
  AssigneeCreateNestedManyWithoutUserInput: ["create", "connectOrCreate", "createMany", "connect"],
  NotificationCreateNestedManyWithoutUser_idInput: ["create", "connectOrCreate", "createMany", "connect"],
  StringFieldUpdateOperationsInput: ["set"],
  NullableStringFieldUpdateOperationsInput: ["set"],
  NullableBoolFieldUpdateOperationsInput: ["set"],
  NullableDateTimeFieldUpdateOperationsInput: ["set"],
  TaskUpdateManyWithoutOwner_idNestedInput: ["create", "connectOrCreate", "upsert", "createMany", "set", "disconnect", "delete", "connect", "update", "updateMany", "deleteMany"],
  CommentUpdateManyWithoutOwner_idNestedInput: ["create", "connectOrCreate", "upsert", "createMany", "set", "disconnect", "delete", "connect", "update", "updateMany", "deleteMany"],
  AssigneeUpdateManyWithoutUserNestedInput: ["create", "connectOrCreate", "upsert", "createMany", "set", "disconnect", "delete", "connect", "update", "updateMany", "deleteMany"],
  NotificationUpdateManyWithoutUser_idNestedInput: ["create", "connectOrCreate", "upsert", "createMany", "set", "disconnect", "delete", "connect", "update", "updateMany", "deleteMany"],
  UserCreateNestedOneWithoutTaskInput: ["create", "connectOrCreate", "connect"],
  CommentCreateNestedManyWithoutTarget_idInput: ["create", "connectOrCreate", "createMany", "connect"],
  AssigneeCreateNestedManyWithoutTask_idInput: ["create", "connectOrCreate", "createMany", "connect"],
  ActionCreateNestedManyWithoutTarget_idInput: ["create", "connectOrCreate", "createMany", "connect"],
  UserUpdateOneWithoutTaskNestedInput: ["create", "connectOrCreate", "upsert", "disconnect", "delete", "connect", "update"],
  EnumTaskStateFieldUpdateOperationsInput: ["set"],
  CommentUpdateManyWithoutTarget_idNestedInput: ["create", "connectOrCreate", "upsert", "createMany", "set", "disconnect", "delete", "connect", "update", "updateMany", "deleteMany"],
  AssigneeUpdateManyWithoutTask_idNestedInput: ["create", "connectOrCreate", "upsert", "createMany", "set", "disconnect", "delete", "connect", "update", "updateMany", "deleteMany"],
  ActionUpdateManyWithoutTarget_idNestedInput: ["create", "connectOrCreate", "upsert", "createMany", "set", "disconnect", "delete", "connect", "update", "updateMany", "deleteMany"],
  UserCreateNestedOneWithoutCommentInput: ["create", "connectOrCreate", "connect"],
  TaskCreateNestedOneWithoutCommentInput: ["create", "connectOrCreate", "connect"],
  UserUpdateOneWithoutCommentNestedInput: ["create", "connectOrCreate", "upsert", "disconnect", "delete", "connect", "update"],
  TaskUpdateOneWithoutCommentNestedInput: ["create", "connectOrCreate", "upsert", "disconnect", "delete", "connect", "update"],
  NullableEnumTargetTypeFieldUpdateOperationsInput: ["set"],
  TaskCreateNestedOneWithoutAssigneesInput: ["create", "connectOrCreate", "connect"],
  UserCreateNestedOneWithoutAssigneesInput: ["create", "connectOrCreate", "connect"],
  TaskUpdateOneWithoutAssigneesNestedInput: ["create", "connectOrCreate", "upsert", "disconnect", "delete", "connect", "update"],
  UserUpdateOneWithoutAssigneesNestedInput: ["create", "connectOrCreate", "upsert", "disconnect", "delete", "connect", "update"],
  TaskCreateNestedOneWithoutActionInput: ["create", "connectOrCreate", "connect"],
  NotificationCreateNestedManyWithoutAction_idInput: ["create", "connectOrCreate", "createMany", "connect"],
  EnumActionGroupFieldUpdateOperationsInput: ["set"],
  EnumActionNameFieldUpdateOperationsInput: ["set"],
  TaskUpdateOneWithoutActionNestedInput: ["create", "connectOrCreate", "upsert", "disconnect", "delete", "connect", "update"],
  EnumTargetTypeFieldUpdateOperationsInput: ["set"],
  NotificationUpdateManyWithoutAction_idNestedInput: ["create", "connectOrCreate", "upsert", "createMany", "set", "disconnect", "delete", "connect", "update", "updateMany", "deleteMany"],
  UserCreateNestedOneWithoutNotificationInput: ["create", "connectOrCreate", "connect"],
  ActionCreateNestedOneWithoutNotificationsInput: ["create", "connectOrCreate", "connect"],
  UserUpdateOneWithoutNotificationNestedInput: ["create", "connectOrCreate", "upsert", "disconnect", "delete", "connect", "update"],
  ActionUpdateOneWithoutNotificationsNestedInput: ["create", "connectOrCreate", "upsert", "disconnect", "delete", "connect", "update"],
  NestedStringFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "contains", "startsWith", "endsWith", "not"],
  NestedStringNullableFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "contains", "startsWith", "endsWith", "not"],
  NestedBoolNullableFilter: ["equals", "not"],
  NestedDateTimeNullableFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "not"],
  NestedStringWithAggregatesFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "contains", "startsWith", "endsWith", "not", "_count", "_min", "_max"],
  NestedIntFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "not"],
  NestedStringNullableWithAggregatesFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "contains", "startsWith", "endsWith", "not", "_count", "_min", "_max"],
  NestedIntNullableFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "not"],
  NestedBoolNullableWithAggregatesFilter: ["equals", "not", "_count", "_min", "_max"],
  NestedJsonNullableFilter: ["equals", "path", "string_contains", "string_starts_with", "string_ends_with", "array_contains", "array_starts_with", "array_ends_with", "lt", "lte", "gt", "gte", "not"],
  NestedDateTimeNullableWithAggregatesFilter: ["equals", "in", "notIn", "lt", "lte", "gt", "gte", "not", "_count", "_min", "_max"],
  NestedEnumTaskStateFilter: ["equals", "in", "notIn", "not"],
  NestedEnumTaskStateWithAggregatesFilter: ["equals", "in", "notIn", "not", "_count", "_min", "_max"],
  NestedEnumTargetTypeNullableFilter: ["equals", "in", "notIn", "not"],
  NestedEnumTargetTypeNullableWithAggregatesFilter: ["equals", "in", "notIn", "not", "_count", "_min", "_max"],
  NestedEnumActionGroupFilter: ["equals", "in", "notIn", "not"],
  NestedEnumActionNameFilter: ["equals", "in", "notIn", "not"],
  NestedEnumTargetTypeFilter: ["equals", "in", "notIn", "not"],
  NestedEnumActionGroupWithAggregatesFilter: ["equals", "in", "notIn", "not", "_count", "_min", "_max"],
  NestedEnumActionNameWithAggregatesFilter: ["equals", "in", "notIn", "not", "_count", "_min", "_max"],
  NestedEnumTargetTypeWithAggregatesFilter: ["equals", "in", "notIn", "not", "_count", "_min", "_max"],
  TaskCreateWithoutOwner_idInput: ["id", "title", "description", "due_at", "created_at", "updated_at", "state", "comment", "assignees", "action"],
  TaskCreateOrConnectWithoutOwner_idInput: ["where", "create"],
  TaskCreateManyOwner_idInputEnvelope: ["data", "skipDuplicates"],
  CommentCreateWithoutOwner_idInput: ["id", "target_id", "target_type", "parent_id", "content", "created_at", "updated_at"],
  CommentCreateOrConnectWithoutOwner_idInput: ["where", "create"],
  CommentCreateManyOwner_idInputEnvelope: ["data", "skipDuplicates"],
  AssigneeCreateWithoutUserInput: ["id", "task_id", "created_at"],
  AssigneeCreateOrConnectWithoutUserInput: ["where", "create"],
  AssigneeCreateManyUserInputEnvelope: ["data", "skipDuplicates"],
  NotificationCreateWithoutUser_idInput: ["id", "action_id", "readed_at"],
  NotificationCreateOrConnectWithoutUser_idInput: ["where", "create"],
  NotificationCreateManyUser_idInputEnvelope: ["data", "skipDuplicates"],
  TaskUpsertWithWhereUniqueWithoutOwner_idInput: ["where", "update", "create"],
  TaskUpdateWithWhereUniqueWithoutOwner_idInput: ["where", "data"],
  TaskUpdateManyWithWhereWithoutOwner_idInput: ["where", "data"],
  TaskScalarWhereInput: ["AND", "OR", "NOT", "id", "title", "description", "due_at", "created_at", "updated_at", "state", "userId"],
  CommentUpsertWithWhereUniqueWithoutOwner_idInput: ["where", "update", "create"],
  CommentUpdateWithWhereUniqueWithoutOwner_idInput: ["where", "data"],
  CommentUpdateManyWithWhereWithoutOwner_idInput: ["where", "data"],
  CommentScalarWhereInput: ["AND", "OR", "NOT", "id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId", "taskId"],
  AssigneeUpsertWithWhereUniqueWithoutUserInput: ["where", "update", "create"],
  AssigneeUpdateWithWhereUniqueWithoutUserInput: ["where", "data"],
  AssigneeUpdateManyWithWhereWithoutUserInput: ["where", "data"],
  AssigneeScalarWhereInput: ["AND", "OR", "NOT", "id", "created_at", "taskId", "userId"],
  NotificationUpsertWithWhereUniqueWithoutUser_idInput: ["where", "update", "create"],
  NotificationUpdateWithWhereUniqueWithoutUser_idInput: ["where", "data"],
  NotificationUpdateManyWithWhereWithoutUser_idInput: ["where", "data"],
  NotificationScalarWhereInput: ["AND", "OR", "NOT", "id", "readed_at", "userId", "actionId"],
  UserCreateWithoutTaskInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "comment", "assignees", "notification"],
  UserCreateOrConnectWithoutTaskInput: ["where", "create"],
  CommentCreateWithoutTarget_idInput: ["id", "owner_id", "target_type", "parent_id", "content", "created_at", "updated_at"],
  CommentCreateOrConnectWithoutTarget_idInput: ["where", "create"],
  CommentCreateManyTarget_idInputEnvelope: ["data", "skipDuplicates"],
  AssigneeCreateWithoutTask_idInput: ["id", "user", "created_at"],
  AssigneeCreateOrConnectWithoutTask_idInput: ["where", "create"],
  AssigneeCreateManyTask_idInputEnvelope: ["data", "skipDuplicates"],
  ActionCreateWithoutTarget_idInput: ["id", "group", "name", "created_at", "target_type", "notifications"],
  ActionCreateOrConnectWithoutTarget_idInput: ["where", "create"],
  ActionCreateManyTarget_idInputEnvelope: ["data", "skipDuplicates"],
  UserUpsertWithoutTaskInput: ["update", "create"],
  UserUpdateWithoutTaskInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "comment", "assignees", "notification"],
  CommentUpsertWithWhereUniqueWithoutTarget_idInput: ["where", "update", "create"],
  CommentUpdateWithWhereUniqueWithoutTarget_idInput: ["where", "data"],
  CommentUpdateManyWithWhereWithoutTarget_idInput: ["where", "data"],
  AssigneeUpsertWithWhereUniqueWithoutTask_idInput: ["where", "update", "create"],
  AssigneeUpdateWithWhereUniqueWithoutTask_idInput: ["where", "data"],
  AssigneeUpdateManyWithWhereWithoutTask_idInput: ["where", "data"],
  ActionUpsertWithWhereUniqueWithoutTarget_idInput: ["where", "update", "create"],
  ActionUpdateWithWhereUniqueWithoutTarget_idInput: ["where", "data"],
  ActionUpdateManyWithWhereWithoutTarget_idInput: ["where", "data"],
  ActionScalarWhereInput: ["AND", "OR", "NOT", "id", "group", "name", "created_at", "target_type", "taskId"],
  UserCreateWithoutCommentInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "task", "assignees", "notification"],
  UserCreateOrConnectWithoutCommentInput: ["where", "create"],
  TaskCreateWithoutCommentInput: ["id", "title", "description", "owner_id", "due_at", "created_at", "updated_at", "state", "assignees", "action"],
  TaskCreateOrConnectWithoutCommentInput: ["where", "create"],
  UserUpsertWithoutCommentInput: ["update", "create"],
  UserUpdateWithoutCommentInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "task", "assignees", "notification"],
  TaskUpsertWithoutCommentInput: ["update", "create"],
  TaskUpdateWithoutCommentInput: ["id", "title", "description", "owner_id", "due_at", "created_at", "updated_at", "state", "assignees", "action"],
  TaskCreateWithoutAssigneesInput: ["id", "title", "description", "owner_id", "due_at", "created_at", "updated_at", "state", "comment", "action"],
  TaskCreateOrConnectWithoutAssigneesInput: ["where", "create"],
  UserCreateWithoutAssigneesInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "task", "comment", "notification"],
  UserCreateOrConnectWithoutAssigneesInput: ["where", "create"],
  TaskUpsertWithoutAssigneesInput: ["update", "create"],
  TaskUpdateWithoutAssigneesInput: ["id", "title", "description", "owner_id", "due_at", "created_at", "updated_at", "state", "comment", "action"],
  UserUpsertWithoutAssigneesInput: ["update", "create"],
  UserUpdateWithoutAssigneesInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "task", "comment", "notification"],
  TaskCreateWithoutActionInput: ["id", "title", "description", "owner_id", "due_at", "created_at", "updated_at", "state", "comment", "assignees"],
  TaskCreateOrConnectWithoutActionInput: ["where", "create"],
  NotificationCreateWithoutAction_idInput: ["id", "user_id", "readed_at"],
  NotificationCreateOrConnectWithoutAction_idInput: ["where", "create"],
  NotificationCreateManyAction_idInputEnvelope: ["data", "skipDuplicates"],
  TaskUpsertWithoutActionInput: ["update", "create"],
  TaskUpdateWithoutActionInput: ["id", "title", "description", "owner_id", "due_at", "created_at", "updated_at", "state", "comment", "assignees"],
  NotificationUpsertWithWhereUniqueWithoutAction_idInput: ["where", "update", "create"],
  NotificationUpdateWithWhereUniqueWithoutAction_idInput: ["where", "data"],
  NotificationUpdateManyWithWhereWithoutAction_idInput: ["where", "data"],
  UserCreateWithoutNotificationInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "task", "comment", "assignees"],
  UserCreateOrConnectWithoutNotificationInput: ["where", "create"],
  ActionCreateWithoutNotificationsInput: ["id", "group", "name", "created_at", "target_id", "target_type"],
  ActionCreateOrConnectWithoutNotificationsInput: ["where", "create"],
  UserUpsertWithoutNotificationInput: ["update", "create"],
  UserUpdateWithoutNotificationInput: ["id", "name", "password_digest", "active", "preferences", "email", "last_sign_in_at", "created_at", "updated_at", "task", "comment", "assignees"],
  ActionUpsertWithoutNotificationsInput: ["update", "create"],
  ActionUpdateWithoutNotificationsInput: ["id", "group", "name", "created_at", "target_id", "target_type"],
  TaskCreateManyOwner_idInput: ["id", "title", "description", "due_at", "created_at", "updated_at", "state"],
  CommentCreateManyOwner_idInput: ["id", "target_type", "parent_id", "content", "created_at", "updated_at", "taskId"],
  AssigneeCreateManyUserInput: ["id", "created_at", "taskId"],
  NotificationCreateManyUser_idInput: ["id", "readed_at", "actionId"],
  TaskUpdateWithoutOwner_idInput: ["id", "title", "description", "due_at", "created_at", "updated_at", "state", "comment", "assignees", "action"],
  CommentUpdateWithoutOwner_idInput: ["id", "target_id", "target_type", "parent_id", "content", "created_at", "updated_at"],
  AssigneeUpdateWithoutUserInput: ["id", "task_id", "created_at"],
  NotificationUpdateWithoutUser_idInput: ["id", "action_id", "readed_at"],
  CommentCreateManyTarget_idInput: ["id", "target_type", "parent_id", "content", "created_at", "updated_at", "userId"],
  AssigneeCreateManyTask_idInput: ["id", "created_at", "userId"],
  ActionCreateManyTarget_idInput: ["id", "group", "name", "created_at", "target_type"],
  CommentUpdateWithoutTarget_idInput: ["id", "owner_id", "target_type", "parent_id", "content", "created_at", "updated_at"],
  AssigneeUpdateWithoutTask_idInput: ["id", "user", "created_at"],
  ActionUpdateWithoutTarget_idInput: ["id", "group", "name", "created_at", "target_type", "notifications"],
  NotificationCreateManyAction_idInput: ["id", "readed_at", "userId"],
  NotificationUpdateWithoutAction_idInput: ["id", "user_id", "readed_at"]
};

type InputTypesNames = keyof typeof inputTypes;

type InputTypeFieldNames<TInput extends InputTypesNames> = Exclude<
  keyof typeof inputTypes[TInput]["prototype"],
  number | symbol
>;

type InputTypeFieldsConfig<
  TInput extends InputTypesNames
> = FieldsConfig<InputTypeFieldNames<TInput>>;

export type InputTypeConfig<TInput extends InputTypesNames> = {
  class?: ClassDecorator[];
  fields?: InputTypeFieldsConfig<TInput>;
};

export type InputTypesEnhanceMap = {
  [TInput in InputTypesNames]?: InputTypeConfig<TInput>;
};

export function applyInputTypesEnhanceMap(
  inputTypesEnhanceMap: InputTypesEnhanceMap,
) {
  for (const inputTypeEnhanceMapKey of Object.keys(inputTypesEnhanceMap)) {
    const inputTypeName = inputTypeEnhanceMapKey as keyof typeof inputTypesEnhanceMap;
    const typeConfig = inputTypesEnhanceMap[inputTypeName]!;
    const typeClass = inputTypes[inputTypeName];
    const typeTarget = typeClass.prototype;
    applyTypeClassEnhanceConfig(
      typeConfig,
      typeClass,
      typeTarget,
      inputsInfo[inputTypeName as keyof typeof inputsInfo],
    );
  }
}

