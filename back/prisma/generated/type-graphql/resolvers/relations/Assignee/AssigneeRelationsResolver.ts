import * as TypeGraphQL from "type-graphql";
import { Assignee } from "../../../models/Assignee";
import { Task } from "../../../models/Task";
import { User } from "../../../models/User";
import { transformInfoIntoPrismaArgs, getPrismaFromContext, transformCountFieldIntoSelectRelationsCount } from "../../../helpers";

@TypeGraphQL.Resolver(_of => Assignee)
export class AssigneeRelationsResolver {
  @TypeGraphQL.FieldResolver(_type => Task, {
    nullable: true
  })
  async task_id(@TypeGraphQL.Root() assignee: Assignee, @TypeGraphQL.Ctx() ctx: any): Promise<Task | null> {
    return getPrismaFromContext(ctx).assignee.findUnique({
      where: {
        id: assignee.id,
      },
    }).task_id({});
  }

  @TypeGraphQL.FieldResolver(_type => User, {
    nullable: true
  })
  async user(@TypeGraphQL.Root() assignee: Assignee, @TypeGraphQL.Ctx() ctx: any): Promise<User | null> {
    return getPrismaFromContext(ctx).assignee.findUnique({
      where: {
        id: assignee.id,
      },
    }).user({});
  }
}
