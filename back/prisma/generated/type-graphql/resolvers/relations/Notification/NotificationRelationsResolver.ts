import * as TypeGraphQL from "type-graphql";
import { Action } from "../../../models/Action";
import { Notification } from "../../../models/Notification";
import { User } from "../../../models/User";
import { transformInfoIntoPrismaArgs, getPrismaFromContext, transformCountFieldIntoSelectRelationsCount } from "../../../helpers";

@TypeGraphQL.Resolver(_of => Notification)
export class NotificationRelationsResolver {
  @TypeGraphQL.FieldResolver(_type => User, {
    nullable: true
  })
  async user_id(@TypeGraphQL.Root() notification: Notification, @TypeGraphQL.Ctx() ctx: any): Promise<User | null> {
    return getPrismaFromContext(ctx).notification.findUnique({
      where: {
        id: notification.id,
      },
    }).user_id({});
  }

  @TypeGraphQL.FieldResolver(_type => Action, {
    nullable: true
  })
  async action_id(@TypeGraphQL.Root() notification: Notification, @TypeGraphQL.Ctx() ctx: any): Promise<Action | null> {
    return getPrismaFromContext(ctx).notification.findUnique({
      where: {
        id: notification.id,
      },
    }).action_id({});
  }
}
