import * as TypeGraphQL from "type-graphql";
import { Action } from "../../../models/Action";
import { Assignee } from "../../../models/Assignee";
import { Comment } from "../../../models/Comment";
import { Task } from "../../../models/Task";
import { User } from "../../../models/User";
import { TaskActionArgs } from "./args/TaskActionArgs";
import { TaskAssigneesArgs } from "./args/TaskAssigneesArgs";
import { TaskCommentArgs } from "./args/TaskCommentArgs";
import { transformInfoIntoPrismaArgs, getPrismaFromContext, transformCountFieldIntoSelectRelationsCount } from "../../../helpers";

@TypeGraphQL.Resolver(_of => Task)
export class TaskRelationsResolver {
  @TypeGraphQL.FieldResolver(_type => User, {
    nullable: true
  })
  async owner_id(@TypeGraphQL.Root() task: Task, @TypeGraphQL.Ctx() ctx: any): Promise<User | null> {
    return getPrismaFromContext(ctx).task.findUnique({
      where: {
        id: task.id,
      },
    }).owner_id({});
  }

  @TypeGraphQL.FieldResolver(_type => [Comment], {
    nullable: false
  })
  async comment(@TypeGraphQL.Root() task: Task, @TypeGraphQL.Ctx() ctx: any, @TypeGraphQL.Args() args: TaskCommentArgs): Promise<Comment[]> {
    return getPrismaFromContext(ctx).task.findUnique({
      where: {
        id: task.id,
      },
    }).comment(args);
  }

  @TypeGraphQL.FieldResolver(_type => [Assignee], {
    nullable: false
  })
  async assignees(@TypeGraphQL.Root() task: Task, @TypeGraphQL.Ctx() ctx: any, @TypeGraphQL.Args() args: TaskAssigneesArgs): Promise<Assignee[]> {
    return getPrismaFromContext(ctx).task.findUnique({
      where: {
        id: task.id,
      },
    }).assignees(args);
  }

  @TypeGraphQL.FieldResolver(_type => [Action], {
    nullable: false
  })
  async action(@TypeGraphQL.Root() task: Task, @TypeGraphQL.Ctx() ctx: any, @TypeGraphQL.Args() args: TaskActionArgs): Promise<Action[]> {
    return getPrismaFromContext(ctx).task.findUnique({
      where: {
        id: task.id,
      },
    }).action(args);
  }
}
