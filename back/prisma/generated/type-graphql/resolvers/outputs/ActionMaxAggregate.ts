import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionGroup } from "../../enums/ActionGroup";
import { ActionName } from "../../enums/ActionName";
import { TargetType } from "../../enums/TargetType";

@TypeGraphQL.ObjectType("ActionMaxAggregate", {
  isAbstract: true,
  simpleResolvers: true
})
export class ActionMaxAggregate {
  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  id!: string | null;

  @TypeGraphQL.Field(_type => ActionGroup, {
    nullable: true
  })
  group!: "TASK" | "COMMENT" | null;

  @TypeGraphQL.Field(_type => ActionName, {
    nullable: true
  })
  name!: "CREATE" | "ADD" | "UPDATE" | "DELETE" | "COMMENT" | "ASSIGN" | "UNASSIGN" | null;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  created_at!: Date | null;

  @TypeGraphQL.Field(_type => TargetType, {
    nullable: true
  })
  target_type!: "FEATURE" | "BUG" | null;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  taskId!: string | null;
}
