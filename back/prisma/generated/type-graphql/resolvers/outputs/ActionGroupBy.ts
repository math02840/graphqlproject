import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionCountAggregate } from "../outputs/ActionCountAggregate";
import { ActionMaxAggregate } from "../outputs/ActionMaxAggregate";
import { ActionMinAggregate } from "../outputs/ActionMinAggregate";
import { ActionGroup } from "../../enums/ActionGroup";
import { ActionName } from "../../enums/ActionName";
import { TargetType } from "../../enums/TargetType";

@TypeGraphQL.ObjectType("ActionGroupBy", {
  isAbstract: true,
  simpleResolvers: true
})
export class ActionGroupBy {
  @TypeGraphQL.Field(_type => String, {
    nullable: false
  })
  id!: string;

  @TypeGraphQL.Field(_type => ActionGroup, {
    nullable: false
  })
  group!: "TASK" | "COMMENT";

  @TypeGraphQL.Field(_type => ActionName, {
    nullable: false
  })
  name!: "CREATE" | "ADD" | "UPDATE" | "DELETE" | "COMMENT" | "ASSIGN" | "UNASSIGN";

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  created_at!: Date | null;

  @TypeGraphQL.Field(_type => TargetType, {
    nullable: false
  })
  target_type!: "FEATURE" | "BUG";

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  taskId!: string | null;

  @TypeGraphQL.Field(_type => ActionCountAggregate, {
    nullable: true
  })
  _count!: ActionCountAggregate | null;

  @TypeGraphQL.Field(_type => ActionMinAggregate, {
    nullable: true
  })
  _min!: ActionMinAggregate | null;

  @TypeGraphQL.Field(_type => ActionMaxAggregate, {
    nullable: true
  })
  _max!: ActionMaxAggregate | null;
}
