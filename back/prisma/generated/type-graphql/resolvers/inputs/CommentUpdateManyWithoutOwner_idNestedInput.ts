import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { CommentCreateManyOwner_idInputEnvelope } from "../inputs/CommentCreateManyOwner_idInputEnvelope";
import { CommentCreateOrConnectWithoutOwner_idInput } from "../inputs/CommentCreateOrConnectWithoutOwner_idInput";
import { CommentCreateWithoutOwner_idInput } from "../inputs/CommentCreateWithoutOwner_idInput";
import { CommentScalarWhereInput } from "../inputs/CommentScalarWhereInput";
import { CommentUpdateManyWithWhereWithoutOwner_idInput } from "../inputs/CommentUpdateManyWithWhereWithoutOwner_idInput";
import { CommentUpdateWithWhereUniqueWithoutOwner_idInput } from "../inputs/CommentUpdateWithWhereUniqueWithoutOwner_idInput";
import { CommentUpsertWithWhereUniqueWithoutOwner_idInput } from "../inputs/CommentUpsertWithWhereUniqueWithoutOwner_idInput";
import { CommentWhereUniqueInput } from "../inputs/CommentWhereUniqueInput";

@TypeGraphQL.InputType("CommentUpdateManyWithoutOwner_idNestedInput", {
  isAbstract: true
})
export class CommentUpdateManyWithoutOwner_idNestedInput {
  @TypeGraphQL.Field(_type => [CommentCreateWithoutOwner_idInput], {
    nullable: true
  })
  create?: CommentCreateWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentCreateOrConnectWithoutOwner_idInput], {
    nullable: true
  })
  connectOrCreate?: CommentCreateOrConnectWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentUpsertWithWhereUniqueWithoutOwner_idInput], {
    nullable: true
  })
  upsert?: CommentUpsertWithWhereUniqueWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => CommentCreateManyOwner_idInputEnvelope, {
    nullable: true
  })
  createMany?: CommentCreateManyOwner_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [CommentWhereUniqueInput], {
    nullable: true
  })
  set?: CommentWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentWhereUniqueInput], {
    nullable: true
  })
  disconnect?: CommentWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentWhereUniqueInput], {
    nullable: true
  })
  delete?: CommentWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentWhereUniqueInput], {
    nullable: true
  })
  connect?: CommentWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentUpdateWithWhereUniqueWithoutOwner_idInput], {
    nullable: true
  })
  update?: CommentUpdateWithWhereUniqueWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentUpdateManyWithWhereWithoutOwner_idInput], {
    nullable: true
  })
  updateMany?: CommentUpdateManyWithWhereWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentScalarWhereInput], {
    nullable: true
  })
  deleteMany?: CommentScalarWhereInput[] | undefined;
}
