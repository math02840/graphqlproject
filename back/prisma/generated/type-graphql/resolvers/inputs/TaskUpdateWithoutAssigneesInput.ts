import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionUpdateManyWithoutTarget_idNestedInput } from "../inputs/ActionUpdateManyWithoutTarget_idNestedInput";
import { CommentUpdateManyWithoutTarget_idNestedInput } from "../inputs/CommentUpdateManyWithoutTarget_idNestedInput";
import { EnumTaskStateFieldUpdateOperationsInput } from "../inputs/EnumTaskStateFieldUpdateOperationsInput";
import { NullableDateTimeFieldUpdateOperationsInput } from "../inputs/NullableDateTimeFieldUpdateOperationsInput";
import { NullableStringFieldUpdateOperationsInput } from "../inputs/NullableStringFieldUpdateOperationsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";
import { UserUpdateOneWithoutTaskNestedInput } from "../inputs/UserUpdateOneWithoutTaskNestedInput";

@TypeGraphQL.InputType("TaskUpdateWithoutAssigneesInput", {
  isAbstract: true
})
export class TaskUpdateWithoutAssigneesInput {
  @TypeGraphQL.Field(_type => StringFieldUpdateOperationsInput, {
    nullable: true
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableStringFieldUpdateOperationsInput, {
    nullable: true
  })
  title?: NullableStringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableStringFieldUpdateOperationsInput, {
    nullable: true
  })
  description?: NullableStringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => UserUpdateOneWithoutTaskNestedInput, {
    nullable: true
  })
  owner_id?: UserUpdateOneWithoutTaskNestedInput | undefined;

  @TypeGraphQL.Field(_type => NullableDateTimeFieldUpdateOperationsInput, {
    nullable: true
  })
  due_at?: NullableDateTimeFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableDateTimeFieldUpdateOperationsInput, {
    nullable: true
  })
  created_at?: NullableDateTimeFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableDateTimeFieldUpdateOperationsInput, {
    nullable: true
  })
  updated_at?: NullableDateTimeFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => EnumTaskStateFieldUpdateOperationsInput, {
    nullable: true
  })
  state?: EnumTaskStateFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => CommentUpdateManyWithoutTarget_idNestedInput, {
    nullable: true
  })
  comment?: CommentUpdateManyWithoutTarget_idNestedInput | undefined;

  @TypeGraphQL.Field(_type => ActionUpdateManyWithoutTarget_idNestedInput, {
    nullable: true
  })
  action?: ActionUpdateManyWithoutTarget_idNestedInput | undefined;
}
