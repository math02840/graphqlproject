import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { CommentCreateWithoutOwner_idInput } from "../inputs/CommentCreateWithoutOwner_idInput";
import { CommentWhereUniqueInput } from "../inputs/CommentWhereUniqueInput";

@TypeGraphQL.InputType("CommentCreateOrConnectWithoutOwner_idInput", {
  isAbstract: true
})
export class CommentCreateOrConnectWithoutOwner_idInput {
  @TypeGraphQL.Field(_type => CommentWhereUniqueInput, {
    nullable: false
  })
  where!: CommentWhereUniqueInput;

  @TypeGraphQL.Field(_type => CommentCreateWithoutOwner_idInput, {
    nullable: false
  })
  create!: CommentCreateWithoutOwner_idInput;
}
