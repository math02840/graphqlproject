import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TargetType } from "../../enums/TargetType";

@TypeGraphQL.InputType("NestedEnumTargetTypeFilter", {
  isAbstract: true
})
export class NestedEnumTargetTypeFilter {
  @TypeGraphQL.Field(_type => TargetType, {
    nullable: true
  })
  equals?: "FEATURE" | "BUG" | undefined;

  @TypeGraphQL.Field(_type => [TargetType], {
    nullable: true
  })
  in?: Array<"FEATURE" | "BUG"> | undefined;

  @TypeGraphQL.Field(_type => [TargetType], {
    nullable: true
  })
  notIn?: Array<"FEATURE" | "BUG"> | undefined;

  @TypeGraphQL.Field(_type => NestedEnumTargetTypeFilter, {
    nullable: true
  })
  not?: NestedEnumTargetTypeFilter | undefined;
}
