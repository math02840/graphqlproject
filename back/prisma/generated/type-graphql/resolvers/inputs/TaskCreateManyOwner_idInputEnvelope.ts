import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TaskCreateManyOwner_idInput } from "../inputs/TaskCreateManyOwner_idInput";

@TypeGraphQL.InputType("TaskCreateManyOwner_idInputEnvelope", {
  isAbstract: true
})
export class TaskCreateManyOwner_idInputEnvelope {
  @TypeGraphQL.Field(_type => [TaskCreateManyOwner_idInput], {
    nullable: false
  })
  data!: TaskCreateManyOwner_idInput[];

  @TypeGraphQL.Field(_type => Boolean, {
    nullable: true
  })
  skipDuplicates?: boolean | undefined;
}
