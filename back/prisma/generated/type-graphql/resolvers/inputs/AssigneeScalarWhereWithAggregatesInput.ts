import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { DateTimeNullableWithAggregatesFilter } from "../inputs/DateTimeNullableWithAggregatesFilter";
import { StringNullableWithAggregatesFilter } from "../inputs/StringNullableWithAggregatesFilter";
import { StringWithAggregatesFilter } from "../inputs/StringWithAggregatesFilter";

@TypeGraphQL.InputType("AssigneeScalarWhereWithAggregatesInput", {
  isAbstract: true
})
export class AssigneeScalarWhereWithAggregatesInput {
  @TypeGraphQL.Field(_type => [AssigneeScalarWhereWithAggregatesInput], {
    nullable: true
  })
  AND?: AssigneeScalarWhereWithAggregatesInput[] | undefined;

  @TypeGraphQL.Field(_type => [AssigneeScalarWhereWithAggregatesInput], {
    nullable: true
  })
  OR?: AssigneeScalarWhereWithAggregatesInput[] | undefined;

  @TypeGraphQL.Field(_type => [AssigneeScalarWhereWithAggregatesInput], {
    nullable: true
  })
  NOT?: AssigneeScalarWhereWithAggregatesInput[] | undefined;

  @TypeGraphQL.Field(_type => StringWithAggregatesFilter, {
    nullable: true
  })
  id?: StringWithAggregatesFilter | undefined;

  @TypeGraphQL.Field(_type => DateTimeNullableWithAggregatesFilter, {
    nullable: true
  })
  created_at?: DateTimeNullableWithAggregatesFilter | undefined;

  @TypeGraphQL.Field(_type => StringNullableWithAggregatesFilter, {
    nullable: true
  })
  taskId?: StringNullableWithAggregatesFilter | undefined;

  @TypeGraphQL.Field(_type => StringNullableWithAggregatesFilter, {
    nullable: true
  })
  userId?: StringNullableWithAggregatesFilter | undefined;
}
