import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionCreateNestedManyWithoutTarget_idInput } from "../inputs/ActionCreateNestedManyWithoutTarget_idInput";
import { CommentCreateNestedManyWithoutTarget_idInput } from "../inputs/CommentCreateNestedManyWithoutTarget_idInput";
import { UserCreateNestedOneWithoutTaskInput } from "../inputs/UserCreateNestedOneWithoutTaskInput";
import { TaskState } from "../../enums/TaskState";

@TypeGraphQL.InputType("TaskCreateWithoutAssigneesInput", {
  isAbstract: true
})
export class TaskCreateWithoutAssigneesInput {
  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  id?: string | undefined;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  title?: string | undefined;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  description?: string | undefined;

  @TypeGraphQL.Field(_type => UserCreateNestedOneWithoutTaskInput, {
    nullable: true
  })
  owner_id?: UserCreateNestedOneWithoutTaskInput | undefined;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  due_at?: Date | undefined;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  created_at?: Date | undefined;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  updated_at?: Date | undefined;

  @TypeGraphQL.Field(_type => TaskState, {
    nullable: false
  })
  state!: "TODO" | "IN_PROGRESS" | "DONE";

  @TypeGraphQL.Field(_type => CommentCreateNestedManyWithoutTarget_idInput, {
    nullable: true
  })
  comment?: CommentCreateNestedManyWithoutTarget_idInput | undefined;

  @TypeGraphQL.Field(_type => ActionCreateNestedManyWithoutTarget_idInput, {
    nullable: true
  })
  action?: ActionCreateNestedManyWithoutTarget_idInput | undefined;
}
