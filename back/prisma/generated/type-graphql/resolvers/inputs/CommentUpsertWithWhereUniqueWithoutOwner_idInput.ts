import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { CommentCreateWithoutOwner_idInput } from "../inputs/CommentCreateWithoutOwner_idInput";
import { CommentUpdateWithoutOwner_idInput } from "../inputs/CommentUpdateWithoutOwner_idInput";
import { CommentWhereUniqueInput } from "../inputs/CommentWhereUniqueInput";

@TypeGraphQL.InputType("CommentUpsertWithWhereUniqueWithoutOwner_idInput", {
  isAbstract: true
})
export class CommentUpsertWithWhereUniqueWithoutOwner_idInput {
  @TypeGraphQL.Field(_type => CommentWhereUniqueInput, {
    nullable: false
  })
  where!: CommentWhereUniqueInput;

  @TypeGraphQL.Field(_type => CommentUpdateWithoutOwner_idInput, {
    nullable: false
  })
  update!: CommentUpdateWithoutOwner_idInput;

  @TypeGraphQL.Field(_type => CommentCreateWithoutOwner_idInput, {
    nullable: false
  })
  create!: CommentCreateWithoutOwner_idInput;
}
