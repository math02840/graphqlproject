import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NotificationCreateManyAction_idInputEnvelope } from "../inputs/NotificationCreateManyAction_idInputEnvelope";
import { NotificationCreateOrConnectWithoutAction_idInput } from "../inputs/NotificationCreateOrConnectWithoutAction_idInput";
import { NotificationCreateWithoutAction_idInput } from "../inputs/NotificationCreateWithoutAction_idInput";
import { NotificationWhereUniqueInput } from "../inputs/NotificationWhereUniqueInput";

@TypeGraphQL.InputType("NotificationCreateNestedManyWithoutAction_idInput", {
  isAbstract: true
})
export class NotificationCreateNestedManyWithoutAction_idInput {
  @TypeGraphQL.Field(_type => [NotificationCreateWithoutAction_idInput], {
    nullable: true
  })
  create?: NotificationCreateWithoutAction_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationCreateOrConnectWithoutAction_idInput], {
    nullable: true
  })
  connectOrCreate?: NotificationCreateOrConnectWithoutAction_idInput[] | undefined;

  @TypeGraphQL.Field(_type => NotificationCreateManyAction_idInputEnvelope, {
    nullable: true
  })
  createMany?: NotificationCreateManyAction_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [NotificationWhereUniqueInput], {
    nullable: true
  })
  connect?: NotificationWhereUniqueInput[] | undefined;
}
