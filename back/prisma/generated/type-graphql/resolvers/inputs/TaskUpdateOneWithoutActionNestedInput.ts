import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TaskCreateOrConnectWithoutActionInput } from "../inputs/TaskCreateOrConnectWithoutActionInput";
import { TaskCreateWithoutActionInput } from "../inputs/TaskCreateWithoutActionInput";
import { TaskUpdateWithoutActionInput } from "../inputs/TaskUpdateWithoutActionInput";
import { TaskUpsertWithoutActionInput } from "../inputs/TaskUpsertWithoutActionInput";
import { TaskWhereUniqueInput } from "../inputs/TaskWhereUniqueInput";

@TypeGraphQL.InputType("TaskUpdateOneWithoutActionNestedInput", {
  isAbstract: true
})
export class TaskUpdateOneWithoutActionNestedInput {
  @TypeGraphQL.Field(_type => TaskCreateWithoutActionInput, {
    nullable: true
  })
  create?: TaskCreateWithoutActionInput | undefined;

  @TypeGraphQL.Field(_type => TaskCreateOrConnectWithoutActionInput, {
    nullable: true
  })
  connectOrCreate?: TaskCreateOrConnectWithoutActionInput | undefined;

  @TypeGraphQL.Field(_type => TaskUpsertWithoutActionInput, {
    nullable: true
  })
  upsert?: TaskUpsertWithoutActionInput | undefined;

  @TypeGraphQL.Field(_type => Boolean, {
    nullable: true
  })
  disconnect?: boolean | undefined;

  @TypeGraphQL.Field(_type => Boolean, {
    nullable: true
  })
  delete?: boolean | undefined;

  @TypeGraphQL.Field(_type => TaskWhereUniqueInput, {
    nullable: true
  })
  connect?: TaskWhereUniqueInput | undefined;

  @TypeGraphQL.Field(_type => TaskUpdateWithoutActionInput, {
    nullable: true
  })
  update?: TaskUpdateWithoutActionInput | undefined;
}
