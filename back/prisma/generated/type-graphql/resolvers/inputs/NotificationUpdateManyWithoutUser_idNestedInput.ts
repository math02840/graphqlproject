import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NotificationCreateManyUser_idInputEnvelope } from "../inputs/NotificationCreateManyUser_idInputEnvelope";
import { NotificationCreateOrConnectWithoutUser_idInput } from "../inputs/NotificationCreateOrConnectWithoutUser_idInput";
import { NotificationCreateWithoutUser_idInput } from "../inputs/NotificationCreateWithoutUser_idInput";
import { NotificationScalarWhereInput } from "../inputs/NotificationScalarWhereInput";
import { NotificationUpdateManyWithWhereWithoutUser_idInput } from "../inputs/NotificationUpdateManyWithWhereWithoutUser_idInput";
import { NotificationUpdateWithWhereUniqueWithoutUser_idInput } from "../inputs/NotificationUpdateWithWhereUniqueWithoutUser_idInput";
import { NotificationUpsertWithWhereUniqueWithoutUser_idInput } from "../inputs/NotificationUpsertWithWhereUniqueWithoutUser_idInput";
import { NotificationWhereUniqueInput } from "../inputs/NotificationWhereUniqueInput";

@TypeGraphQL.InputType("NotificationUpdateManyWithoutUser_idNestedInput", {
  isAbstract: true
})
export class NotificationUpdateManyWithoutUser_idNestedInput {
  @TypeGraphQL.Field(_type => [NotificationCreateWithoutUser_idInput], {
    nullable: true
  })
  create?: NotificationCreateWithoutUser_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationCreateOrConnectWithoutUser_idInput], {
    nullable: true
  })
  connectOrCreate?: NotificationCreateOrConnectWithoutUser_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationUpsertWithWhereUniqueWithoutUser_idInput], {
    nullable: true
  })
  upsert?: NotificationUpsertWithWhereUniqueWithoutUser_idInput[] | undefined;

  @TypeGraphQL.Field(_type => NotificationCreateManyUser_idInputEnvelope, {
    nullable: true
  })
  createMany?: NotificationCreateManyUser_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [NotificationWhereUniqueInput], {
    nullable: true
  })
  set?: NotificationWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationWhereUniqueInput], {
    nullable: true
  })
  disconnect?: NotificationWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationWhereUniqueInput], {
    nullable: true
  })
  delete?: NotificationWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationWhereUniqueInput], {
    nullable: true
  })
  connect?: NotificationWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationUpdateWithWhereUniqueWithoutUser_idInput], {
    nullable: true
  })
  update?: NotificationUpdateWithWhereUniqueWithoutUser_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationUpdateManyWithWhereWithoutUser_idInput], {
    nullable: true
  })
  updateMany?: NotificationUpdateManyWithWhereWithoutUser_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationScalarWhereInput], {
    nullable: true
  })
  deleteMany?: NotificationScalarWhereInput[] | undefined;
}
