import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionName } from "../../enums/ActionName";

@TypeGraphQL.InputType("NestedEnumActionNameFilter", {
  isAbstract: true
})
export class NestedEnumActionNameFilter {
  @TypeGraphQL.Field(_type => ActionName, {
    nullable: true
  })
  equals?: "CREATE" | "ADD" | "UPDATE" | "DELETE" | "COMMENT" | "ASSIGN" | "UNASSIGN" | undefined;

  @TypeGraphQL.Field(_type => [ActionName], {
    nullable: true
  })
  in?: Array<"CREATE" | "ADD" | "UPDATE" | "DELETE" | "COMMENT" | "ASSIGN" | "UNASSIGN"> | undefined;

  @TypeGraphQL.Field(_type => [ActionName], {
    nullable: true
  })
  notIn?: Array<"CREATE" | "ADD" | "UPDATE" | "DELETE" | "COMMENT" | "ASSIGN" | "UNASSIGN"> | undefined;

  @TypeGraphQL.Field(_type => NestedEnumActionNameFilter, {
    nullable: true
  })
  not?: NestedEnumActionNameFilter | undefined;
}
