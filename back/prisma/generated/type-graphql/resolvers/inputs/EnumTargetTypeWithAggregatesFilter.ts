import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NestedEnumTargetTypeFilter } from "../inputs/NestedEnumTargetTypeFilter";
import { NestedEnumTargetTypeWithAggregatesFilter } from "../inputs/NestedEnumTargetTypeWithAggregatesFilter";
import { NestedIntFilter } from "../inputs/NestedIntFilter";
import { TargetType } from "../../enums/TargetType";

@TypeGraphQL.InputType("EnumTargetTypeWithAggregatesFilter", {
  isAbstract: true
})
export class EnumTargetTypeWithAggregatesFilter {
  @TypeGraphQL.Field(_type => TargetType, {
    nullable: true
  })
  equals?: "FEATURE" | "BUG" | undefined;

  @TypeGraphQL.Field(_type => [TargetType], {
    nullable: true
  })
  in?: Array<"FEATURE" | "BUG"> | undefined;

  @TypeGraphQL.Field(_type => [TargetType], {
    nullable: true
  })
  notIn?: Array<"FEATURE" | "BUG"> | undefined;

  @TypeGraphQL.Field(_type => NestedEnumTargetTypeWithAggregatesFilter, {
    nullable: true
  })
  not?: NestedEnumTargetTypeWithAggregatesFilter | undefined;

  @TypeGraphQL.Field(_type => NestedIntFilter, {
    nullable: true
  })
  _count?: NestedIntFilter | undefined;

  @TypeGraphQL.Field(_type => NestedEnumTargetTypeFilter, {
    nullable: true
  })
  _min?: NestedEnumTargetTypeFilter | undefined;

  @TypeGraphQL.Field(_type => NestedEnumTargetTypeFilter, {
    nullable: true
  })
  _max?: NestedEnumTargetTypeFilter | undefined;
}
