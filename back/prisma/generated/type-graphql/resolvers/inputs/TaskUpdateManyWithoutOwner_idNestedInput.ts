import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TaskCreateManyOwner_idInputEnvelope } from "../inputs/TaskCreateManyOwner_idInputEnvelope";
import { TaskCreateOrConnectWithoutOwner_idInput } from "../inputs/TaskCreateOrConnectWithoutOwner_idInput";
import { TaskCreateWithoutOwner_idInput } from "../inputs/TaskCreateWithoutOwner_idInput";
import { TaskScalarWhereInput } from "../inputs/TaskScalarWhereInput";
import { TaskUpdateManyWithWhereWithoutOwner_idInput } from "../inputs/TaskUpdateManyWithWhereWithoutOwner_idInput";
import { TaskUpdateWithWhereUniqueWithoutOwner_idInput } from "../inputs/TaskUpdateWithWhereUniqueWithoutOwner_idInput";
import { TaskUpsertWithWhereUniqueWithoutOwner_idInput } from "../inputs/TaskUpsertWithWhereUniqueWithoutOwner_idInput";
import { TaskWhereUniqueInput } from "../inputs/TaskWhereUniqueInput";

@TypeGraphQL.InputType("TaskUpdateManyWithoutOwner_idNestedInput", {
  isAbstract: true
})
export class TaskUpdateManyWithoutOwner_idNestedInput {
  @TypeGraphQL.Field(_type => [TaskCreateWithoutOwner_idInput], {
    nullable: true
  })
  create?: TaskCreateWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [TaskCreateOrConnectWithoutOwner_idInput], {
    nullable: true
  })
  connectOrCreate?: TaskCreateOrConnectWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [TaskUpsertWithWhereUniqueWithoutOwner_idInput], {
    nullable: true
  })
  upsert?: TaskUpsertWithWhereUniqueWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => TaskCreateManyOwner_idInputEnvelope, {
    nullable: true
  })
  createMany?: TaskCreateManyOwner_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [TaskWhereUniqueInput], {
    nullable: true
  })
  set?: TaskWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [TaskWhereUniqueInput], {
    nullable: true
  })
  disconnect?: TaskWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [TaskWhereUniqueInput], {
    nullable: true
  })
  delete?: TaskWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [TaskWhereUniqueInput], {
    nullable: true
  })
  connect?: TaskWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [TaskUpdateWithWhereUniqueWithoutOwner_idInput], {
    nullable: true
  })
  update?: TaskUpdateWithWhereUniqueWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [TaskUpdateManyWithWhereWithoutOwner_idInput], {
    nullable: true
  })
  updateMany?: TaskUpdateManyWithWhereWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [TaskScalarWhereInput], {
    nullable: true
  })
  deleteMany?: TaskScalarWhereInput[] | undefined;
}
