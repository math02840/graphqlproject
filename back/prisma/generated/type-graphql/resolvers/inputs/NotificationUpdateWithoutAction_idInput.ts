import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NullableDateTimeFieldUpdateOperationsInput } from "../inputs/NullableDateTimeFieldUpdateOperationsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";
import { UserUpdateOneWithoutNotificationNestedInput } from "../inputs/UserUpdateOneWithoutNotificationNestedInput";

@TypeGraphQL.InputType("NotificationUpdateWithoutAction_idInput", {
  isAbstract: true
})
export class NotificationUpdateWithoutAction_idInput {
  @TypeGraphQL.Field(_type => StringFieldUpdateOperationsInput, {
    nullable: true
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => UserUpdateOneWithoutNotificationNestedInput, {
    nullable: true
  })
  user_id?: UserUpdateOneWithoutNotificationNestedInput | undefined;

  @TypeGraphQL.Field(_type => NullableDateTimeFieldUpdateOperationsInput, {
    nullable: true
  })
  readed_at?: NullableDateTimeFieldUpdateOperationsInput | undefined;
}
