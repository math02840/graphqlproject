import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NotificationUpdateWithoutAction_idInput } from "../inputs/NotificationUpdateWithoutAction_idInput";
import { NotificationWhereUniqueInput } from "../inputs/NotificationWhereUniqueInput";

@TypeGraphQL.InputType("NotificationUpdateWithWhereUniqueWithoutAction_idInput", {
  isAbstract: true
})
export class NotificationUpdateWithWhereUniqueWithoutAction_idInput {
  @TypeGraphQL.Field(_type => NotificationWhereUniqueInput, {
    nullable: false
  })
  where!: NotificationWhereUniqueInput;

  @TypeGraphQL.Field(_type => NotificationUpdateWithoutAction_idInput, {
    nullable: false
  })
  data!: NotificationUpdateWithoutAction_idInput;
}
