import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TaskCreateOrConnectWithoutCommentInput } from "../inputs/TaskCreateOrConnectWithoutCommentInput";
import { TaskCreateWithoutCommentInput } from "../inputs/TaskCreateWithoutCommentInput";
import { TaskWhereUniqueInput } from "../inputs/TaskWhereUniqueInput";

@TypeGraphQL.InputType("TaskCreateNestedOneWithoutCommentInput", {
  isAbstract: true
})
export class TaskCreateNestedOneWithoutCommentInput {
  @TypeGraphQL.Field(_type => TaskCreateWithoutCommentInput, {
    nullable: true
  })
  create?: TaskCreateWithoutCommentInput | undefined;

  @TypeGraphQL.Field(_type => TaskCreateOrConnectWithoutCommentInput, {
    nullable: true
  })
  connectOrCreate?: TaskCreateOrConnectWithoutCommentInput | undefined;

  @TypeGraphQL.Field(_type => TaskWhereUniqueInput, {
    nullable: true
  })
  connect?: TaskWhereUniqueInput | undefined;
}
