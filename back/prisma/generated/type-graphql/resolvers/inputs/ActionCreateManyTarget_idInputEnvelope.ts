import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionCreateManyTarget_idInput } from "../inputs/ActionCreateManyTarget_idInput";

@TypeGraphQL.InputType("ActionCreateManyTarget_idInputEnvelope", {
  isAbstract: true
})
export class ActionCreateManyTarget_idInputEnvelope {
  @TypeGraphQL.Field(_type => [ActionCreateManyTarget_idInput], {
    nullable: false
  })
  data!: ActionCreateManyTarget_idInput[];

  @TypeGraphQL.Field(_type => Boolean, {
    nullable: true
  })
  skipDuplicates?: boolean | undefined;
}
