import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NotificationCreateManyAction_idInput } from "../inputs/NotificationCreateManyAction_idInput";

@TypeGraphQL.InputType("NotificationCreateManyAction_idInputEnvelope", {
  isAbstract: true
})
export class NotificationCreateManyAction_idInputEnvelope {
  @TypeGraphQL.Field(_type => [NotificationCreateManyAction_idInput], {
    nullable: false
  })
  data!: NotificationCreateManyAction_idInput[];

  @TypeGraphQL.Field(_type => Boolean, {
    nullable: true
  })
  skipDuplicates?: boolean | undefined;
}
