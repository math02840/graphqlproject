import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { CommentCreateManyTarget_idInputEnvelope } from "../inputs/CommentCreateManyTarget_idInputEnvelope";
import { CommentCreateOrConnectWithoutTarget_idInput } from "../inputs/CommentCreateOrConnectWithoutTarget_idInput";
import { CommentCreateWithoutTarget_idInput } from "../inputs/CommentCreateWithoutTarget_idInput";
import { CommentScalarWhereInput } from "../inputs/CommentScalarWhereInput";
import { CommentUpdateManyWithWhereWithoutTarget_idInput } from "../inputs/CommentUpdateManyWithWhereWithoutTarget_idInput";
import { CommentUpdateWithWhereUniqueWithoutTarget_idInput } from "../inputs/CommentUpdateWithWhereUniqueWithoutTarget_idInput";
import { CommentUpsertWithWhereUniqueWithoutTarget_idInput } from "../inputs/CommentUpsertWithWhereUniqueWithoutTarget_idInput";
import { CommentWhereUniqueInput } from "../inputs/CommentWhereUniqueInput";

@TypeGraphQL.InputType("CommentUpdateManyWithoutTarget_idNestedInput", {
  isAbstract: true
})
export class CommentUpdateManyWithoutTarget_idNestedInput {
  @TypeGraphQL.Field(_type => [CommentCreateWithoutTarget_idInput], {
    nullable: true
  })
  create?: CommentCreateWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentCreateOrConnectWithoutTarget_idInput], {
    nullable: true
  })
  connectOrCreate?: CommentCreateOrConnectWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentUpsertWithWhereUniqueWithoutTarget_idInput], {
    nullable: true
  })
  upsert?: CommentUpsertWithWhereUniqueWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => CommentCreateManyTarget_idInputEnvelope, {
    nullable: true
  })
  createMany?: CommentCreateManyTarget_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [CommentWhereUniqueInput], {
    nullable: true
  })
  set?: CommentWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentWhereUniqueInput], {
    nullable: true
  })
  disconnect?: CommentWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentWhereUniqueInput], {
    nullable: true
  })
  delete?: CommentWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentWhereUniqueInput], {
    nullable: true
  })
  connect?: CommentWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentUpdateWithWhereUniqueWithoutTarget_idInput], {
    nullable: true
  })
  update?: CommentUpdateWithWhereUniqueWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentUpdateManyWithWhereWithoutTarget_idInput], {
    nullable: true
  })
  updateMany?: CommentUpdateManyWithWhereWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentScalarWhereInput], {
    nullable: true
  })
  deleteMany?: CommentScalarWhereInput[] | undefined;
}
