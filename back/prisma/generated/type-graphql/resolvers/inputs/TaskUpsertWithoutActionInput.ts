import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TaskCreateWithoutActionInput } from "../inputs/TaskCreateWithoutActionInput";
import { TaskUpdateWithoutActionInput } from "../inputs/TaskUpdateWithoutActionInput";

@TypeGraphQL.InputType("TaskUpsertWithoutActionInput", {
  isAbstract: true
})
export class TaskUpsertWithoutActionInput {
  @TypeGraphQL.Field(_type => TaskUpdateWithoutActionInput, {
    nullable: false
  })
  update!: TaskUpdateWithoutActionInput;

  @TypeGraphQL.Field(_type => TaskCreateWithoutActionInput, {
    nullable: false
  })
  create!: TaskCreateWithoutActionInput;
}
