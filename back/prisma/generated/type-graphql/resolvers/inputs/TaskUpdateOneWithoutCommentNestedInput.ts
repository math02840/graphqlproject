import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TaskCreateOrConnectWithoutCommentInput } from "../inputs/TaskCreateOrConnectWithoutCommentInput";
import { TaskCreateWithoutCommentInput } from "../inputs/TaskCreateWithoutCommentInput";
import { TaskUpdateWithoutCommentInput } from "../inputs/TaskUpdateWithoutCommentInput";
import { TaskUpsertWithoutCommentInput } from "../inputs/TaskUpsertWithoutCommentInput";
import { TaskWhereUniqueInput } from "../inputs/TaskWhereUniqueInput";

@TypeGraphQL.InputType("TaskUpdateOneWithoutCommentNestedInput", {
  isAbstract: true
})
export class TaskUpdateOneWithoutCommentNestedInput {
  @TypeGraphQL.Field(_type => TaskCreateWithoutCommentInput, {
    nullable: true
  })
  create?: TaskCreateWithoutCommentInput | undefined;

  @TypeGraphQL.Field(_type => TaskCreateOrConnectWithoutCommentInput, {
    nullable: true
  })
  connectOrCreate?: TaskCreateOrConnectWithoutCommentInput | undefined;

  @TypeGraphQL.Field(_type => TaskUpsertWithoutCommentInput, {
    nullable: true
  })
  upsert?: TaskUpsertWithoutCommentInput | undefined;

  @TypeGraphQL.Field(_type => Boolean, {
    nullable: true
  })
  disconnect?: boolean | undefined;

  @TypeGraphQL.Field(_type => Boolean, {
    nullable: true
  })
  delete?: boolean | undefined;

  @TypeGraphQL.Field(_type => TaskWhereUniqueInput, {
    nullable: true
  })
  connect?: TaskWhereUniqueInput | undefined;

  @TypeGraphQL.Field(_type => TaskUpdateWithoutCommentInput, {
    nullable: true
  })
  update?: TaskUpdateWithoutCommentInput | undefined;
}
