import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { AssigneeCreateManyTask_idInputEnvelope } from "../inputs/AssigneeCreateManyTask_idInputEnvelope";
import { AssigneeCreateOrConnectWithoutTask_idInput } from "../inputs/AssigneeCreateOrConnectWithoutTask_idInput";
import { AssigneeCreateWithoutTask_idInput } from "../inputs/AssigneeCreateWithoutTask_idInput";
import { AssigneeWhereUniqueInput } from "../inputs/AssigneeWhereUniqueInput";

@TypeGraphQL.InputType("AssigneeCreateNestedManyWithoutTask_idInput", {
  isAbstract: true
})
export class AssigneeCreateNestedManyWithoutTask_idInput {
  @TypeGraphQL.Field(_type => [AssigneeCreateWithoutTask_idInput], {
    nullable: true
  })
  create?: AssigneeCreateWithoutTask_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [AssigneeCreateOrConnectWithoutTask_idInput], {
    nullable: true
  })
  connectOrCreate?: AssigneeCreateOrConnectWithoutTask_idInput[] | undefined;

  @TypeGraphQL.Field(_type => AssigneeCreateManyTask_idInputEnvelope, {
    nullable: true
  })
  createMany?: AssigneeCreateManyTask_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [AssigneeWhereUniqueInput], {
    nullable: true
  })
  connect?: AssigneeWhereUniqueInput[] | undefined;
}
