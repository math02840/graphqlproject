import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NestedEnumActionNameFilter } from "../inputs/NestedEnumActionNameFilter";
import { NestedEnumActionNameWithAggregatesFilter } from "../inputs/NestedEnumActionNameWithAggregatesFilter";
import { NestedIntFilter } from "../inputs/NestedIntFilter";
import { ActionName } from "../../enums/ActionName";

@TypeGraphQL.InputType("EnumActionNameWithAggregatesFilter", {
  isAbstract: true
})
export class EnumActionNameWithAggregatesFilter {
  @TypeGraphQL.Field(_type => ActionName, {
    nullable: true
  })
  equals?: "CREATE" | "ADD" | "UPDATE" | "DELETE" | "COMMENT" | "ASSIGN" | "UNASSIGN" | undefined;

  @TypeGraphQL.Field(_type => [ActionName], {
    nullable: true
  })
  in?: Array<"CREATE" | "ADD" | "UPDATE" | "DELETE" | "COMMENT" | "ASSIGN" | "UNASSIGN"> | undefined;

  @TypeGraphQL.Field(_type => [ActionName], {
    nullable: true
  })
  notIn?: Array<"CREATE" | "ADD" | "UPDATE" | "DELETE" | "COMMENT" | "ASSIGN" | "UNASSIGN"> | undefined;

  @TypeGraphQL.Field(_type => NestedEnumActionNameWithAggregatesFilter, {
    nullable: true
  })
  not?: NestedEnumActionNameWithAggregatesFilter | undefined;

  @TypeGraphQL.Field(_type => NestedIntFilter, {
    nullable: true
  })
  _count?: NestedIntFilter | undefined;

  @TypeGraphQL.Field(_type => NestedEnumActionNameFilter, {
    nullable: true
  })
  _min?: NestedEnumActionNameFilter | undefined;

  @TypeGraphQL.Field(_type => NestedEnumActionNameFilter, {
    nullable: true
  })
  _max?: NestedEnumActionNameFilter | undefined;
}
