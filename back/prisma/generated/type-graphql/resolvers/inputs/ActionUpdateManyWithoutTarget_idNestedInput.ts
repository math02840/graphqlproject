import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionCreateManyTarget_idInputEnvelope } from "../inputs/ActionCreateManyTarget_idInputEnvelope";
import { ActionCreateOrConnectWithoutTarget_idInput } from "../inputs/ActionCreateOrConnectWithoutTarget_idInput";
import { ActionCreateWithoutTarget_idInput } from "../inputs/ActionCreateWithoutTarget_idInput";
import { ActionScalarWhereInput } from "../inputs/ActionScalarWhereInput";
import { ActionUpdateManyWithWhereWithoutTarget_idInput } from "../inputs/ActionUpdateManyWithWhereWithoutTarget_idInput";
import { ActionUpdateWithWhereUniqueWithoutTarget_idInput } from "../inputs/ActionUpdateWithWhereUniqueWithoutTarget_idInput";
import { ActionUpsertWithWhereUniqueWithoutTarget_idInput } from "../inputs/ActionUpsertWithWhereUniqueWithoutTarget_idInput";
import { ActionWhereUniqueInput } from "../inputs/ActionWhereUniqueInput";

@TypeGraphQL.InputType("ActionUpdateManyWithoutTarget_idNestedInput", {
  isAbstract: true
})
export class ActionUpdateManyWithoutTarget_idNestedInput {
  @TypeGraphQL.Field(_type => [ActionCreateWithoutTarget_idInput], {
    nullable: true
  })
  create?: ActionCreateWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionCreateOrConnectWithoutTarget_idInput], {
    nullable: true
  })
  connectOrCreate?: ActionCreateOrConnectWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionUpsertWithWhereUniqueWithoutTarget_idInput], {
    nullable: true
  })
  upsert?: ActionUpsertWithWhereUniqueWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => ActionCreateManyTarget_idInputEnvelope, {
    nullable: true
  })
  createMany?: ActionCreateManyTarget_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [ActionWhereUniqueInput], {
    nullable: true
  })
  set?: ActionWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionWhereUniqueInput], {
    nullable: true
  })
  disconnect?: ActionWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionWhereUniqueInput], {
    nullable: true
  })
  delete?: ActionWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionWhereUniqueInput], {
    nullable: true
  })
  connect?: ActionWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionUpdateWithWhereUniqueWithoutTarget_idInput], {
    nullable: true
  })
  update?: ActionUpdateWithWhereUniqueWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionUpdateManyWithWhereWithoutTarget_idInput], {
    nullable: true
  })
  updateMany?: ActionUpdateManyWithWhereWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionScalarWhereInput], {
    nullable: true
  })
  deleteMany?: ActionScalarWhereInput[] | undefined;
}
