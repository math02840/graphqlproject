import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TaskCreateWithoutCommentInput } from "../inputs/TaskCreateWithoutCommentInput";
import { TaskUpdateWithoutCommentInput } from "../inputs/TaskUpdateWithoutCommentInput";

@TypeGraphQL.InputType("TaskUpsertWithoutCommentInput", {
  isAbstract: true
})
export class TaskUpsertWithoutCommentInput {
  @TypeGraphQL.Field(_type => TaskUpdateWithoutCommentInput, {
    nullable: false
  })
  update!: TaskUpdateWithoutCommentInput;

  @TypeGraphQL.Field(_type => TaskCreateWithoutCommentInput, {
    nullable: false
  })
  create!: TaskCreateWithoutCommentInput;
}
