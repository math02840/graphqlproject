import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NotificationCreateManyAction_idInputEnvelope } from "../inputs/NotificationCreateManyAction_idInputEnvelope";
import { NotificationCreateOrConnectWithoutAction_idInput } from "../inputs/NotificationCreateOrConnectWithoutAction_idInput";
import { NotificationCreateWithoutAction_idInput } from "../inputs/NotificationCreateWithoutAction_idInput";
import { NotificationScalarWhereInput } from "../inputs/NotificationScalarWhereInput";
import { NotificationUpdateManyWithWhereWithoutAction_idInput } from "../inputs/NotificationUpdateManyWithWhereWithoutAction_idInput";
import { NotificationUpdateWithWhereUniqueWithoutAction_idInput } from "../inputs/NotificationUpdateWithWhereUniqueWithoutAction_idInput";
import { NotificationUpsertWithWhereUniqueWithoutAction_idInput } from "../inputs/NotificationUpsertWithWhereUniqueWithoutAction_idInput";
import { NotificationWhereUniqueInput } from "../inputs/NotificationWhereUniqueInput";

@TypeGraphQL.InputType("NotificationUpdateManyWithoutAction_idNestedInput", {
  isAbstract: true
})
export class NotificationUpdateManyWithoutAction_idNestedInput {
  @TypeGraphQL.Field(_type => [NotificationCreateWithoutAction_idInput], {
    nullable: true
  })
  create?: NotificationCreateWithoutAction_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationCreateOrConnectWithoutAction_idInput], {
    nullable: true
  })
  connectOrCreate?: NotificationCreateOrConnectWithoutAction_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationUpsertWithWhereUniqueWithoutAction_idInput], {
    nullable: true
  })
  upsert?: NotificationUpsertWithWhereUniqueWithoutAction_idInput[] | undefined;

  @TypeGraphQL.Field(_type => NotificationCreateManyAction_idInputEnvelope, {
    nullable: true
  })
  createMany?: NotificationCreateManyAction_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [NotificationWhereUniqueInput], {
    nullable: true
  })
  set?: NotificationWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationWhereUniqueInput], {
    nullable: true
  })
  disconnect?: NotificationWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationWhereUniqueInput], {
    nullable: true
  })
  delete?: NotificationWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationWhereUniqueInput], {
    nullable: true
  })
  connect?: NotificationWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationUpdateWithWhereUniqueWithoutAction_idInput], {
    nullable: true
  })
  update?: NotificationUpdateWithWhereUniqueWithoutAction_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationUpdateManyWithWhereWithoutAction_idInput], {
    nullable: true
  })
  updateMany?: NotificationUpdateManyWithWhereWithoutAction_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationScalarWhereInput], {
    nullable: true
  })
  deleteMany?: NotificationScalarWhereInput[] | undefined;
}
