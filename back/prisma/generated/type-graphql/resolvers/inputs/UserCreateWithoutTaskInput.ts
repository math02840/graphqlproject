import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { AssigneeCreateNestedManyWithoutUserInput } from "../inputs/AssigneeCreateNestedManyWithoutUserInput";
import { CommentCreateNestedManyWithoutOwner_idInput } from "../inputs/CommentCreateNestedManyWithoutOwner_idInput";
import { NotificationCreateNestedManyWithoutUser_idInput } from "../inputs/NotificationCreateNestedManyWithoutUser_idInput";

@TypeGraphQL.InputType("UserCreateWithoutTaskInput", {
  isAbstract: true
})
export class UserCreateWithoutTaskInput {
  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  id?: string | undefined;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  name?: string | undefined;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  password_digest?: string | undefined;

  @TypeGraphQL.Field(_type => Boolean, {
    nullable: true
  })
  active?: boolean | undefined;

  @TypeGraphQL.Field(_type => GraphQLScalars.JSONResolver, {
    nullable: true
  })
  preferences?: Prisma.InputJsonValue | undefined;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  email?: string | undefined;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  last_sign_in_at?: Date | undefined;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  created_at?: Date | undefined;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  updated_at?: Date | undefined;

  @TypeGraphQL.Field(_type => CommentCreateNestedManyWithoutOwner_idInput, {
    nullable: true
  })
  comment?: CommentCreateNestedManyWithoutOwner_idInput | undefined;

  @TypeGraphQL.Field(_type => AssigneeCreateNestedManyWithoutUserInput, {
    nullable: true
  })
  assignees?: AssigneeCreateNestedManyWithoutUserInput | undefined;

  @TypeGraphQL.Field(_type => NotificationCreateNestedManyWithoutUser_idInput, {
    nullable: true
  })
  notification?: NotificationCreateNestedManyWithoutUser_idInput | undefined;
}
