import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionUpdateOneWithoutNotificationsNestedInput } from "../inputs/ActionUpdateOneWithoutNotificationsNestedInput";
import { NullableDateTimeFieldUpdateOperationsInput } from "../inputs/NullableDateTimeFieldUpdateOperationsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";

@TypeGraphQL.InputType("NotificationUpdateWithoutUser_idInput", {
  isAbstract: true
})
export class NotificationUpdateWithoutUser_idInput {
  @TypeGraphQL.Field(_type => StringFieldUpdateOperationsInput, {
    nullable: true
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => ActionUpdateOneWithoutNotificationsNestedInput, {
    nullable: true
  })
  action_id?: ActionUpdateOneWithoutNotificationsNestedInput | undefined;

  @TypeGraphQL.Field(_type => NullableDateTimeFieldUpdateOperationsInput, {
    nullable: true
  })
  readed_at?: NullableDateTimeFieldUpdateOperationsInput | undefined;
}
