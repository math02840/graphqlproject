import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { AssigneeCreateManyTask_idInput } from "../inputs/AssigneeCreateManyTask_idInput";

@TypeGraphQL.InputType("AssigneeCreateManyTask_idInputEnvelope", {
  isAbstract: true
})
export class AssigneeCreateManyTask_idInputEnvelope {
  @TypeGraphQL.Field(_type => [AssigneeCreateManyTask_idInput], {
    nullable: false
  })
  data!: AssigneeCreateManyTask_idInput[];

  @TypeGraphQL.Field(_type => Boolean, {
    nullable: true
  })
  skipDuplicates?: boolean | undefined;
}
