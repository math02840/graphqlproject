import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionCreateManyTarget_idInputEnvelope } from "../inputs/ActionCreateManyTarget_idInputEnvelope";
import { ActionCreateOrConnectWithoutTarget_idInput } from "../inputs/ActionCreateOrConnectWithoutTarget_idInput";
import { ActionCreateWithoutTarget_idInput } from "../inputs/ActionCreateWithoutTarget_idInput";
import { ActionWhereUniqueInput } from "../inputs/ActionWhereUniqueInput";

@TypeGraphQL.InputType("ActionCreateNestedManyWithoutTarget_idInput", {
  isAbstract: true
})
export class ActionCreateNestedManyWithoutTarget_idInput {
  @TypeGraphQL.Field(_type => [ActionCreateWithoutTarget_idInput], {
    nullable: true
  })
  create?: ActionCreateWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionCreateOrConnectWithoutTarget_idInput], {
    nullable: true
  })
  connectOrCreate?: ActionCreateOrConnectWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => ActionCreateManyTarget_idInputEnvelope, {
    nullable: true
  })
  createMany?: ActionCreateManyTarget_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [ActionWhereUniqueInput], {
    nullable: true
  })
  connect?: ActionWhereUniqueInput[] | undefined;
}
