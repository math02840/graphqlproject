import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { CommentCreateWithoutTarget_idInput } from "../inputs/CommentCreateWithoutTarget_idInput";
import { CommentWhereUniqueInput } from "../inputs/CommentWhereUniqueInput";

@TypeGraphQL.InputType("CommentCreateOrConnectWithoutTarget_idInput", {
  isAbstract: true
})
export class CommentCreateOrConnectWithoutTarget_idInput {
  @TypeGraphQL.Field(_type => CommentWhereUniqueInput, {
    nullable: false
  })
  where!: CommentWhereUniqueInput;

  @TypeGraphQL.Field(_type => CommentCreateWithoutTarget_idInput, {
    nullable: false
  })
  create!: CommentCreateWithoutTarget_idInput;
}
