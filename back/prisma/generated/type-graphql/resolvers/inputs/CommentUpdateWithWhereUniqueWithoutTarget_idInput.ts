import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { CommentUpdateWithoutTarget_idInput } from "../inputs/CommentUpdateWithoutTarget_idInput";
import { CommentWhereUniqueInput } from "../inputs/CommentWhereUniqueInput";

@TypeGraphQL.InputType("CommentUpdateWithWhereUniqueWithoutTarget_idInput", {
  isAbstract: true
})
export class CommentUpdateWithWhereUniqueWithoutTarget_idInput {
  @TypeGraphQL.Field(_type => CommentWhereUniqueInput, {
    nullable: false
  })
  where!: CommentWhereUniqueInput;

  @TypeGraphQL.Field(_type => CommentUpdateWithoutTarget_idInput, {
    nullable: false
  })
  data!: CommentUpdateWithoutTarget_idInput;
}
