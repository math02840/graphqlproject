import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NotificationCreateWithoutAction_idInput } from "../inputs/NotificationCreateWithoutAction_idInput";
import { NotificationUpdateWithoutAction_idInput } from "../inputs/NotificationUpdateWithoutAction_idInput";
import { NotificationWhereUniqueInput } from "../inputs/NotificationWhereUniqueInput";

@TypeGraphQL.InputType("NotificationUpsertWithWhereUniqueWithoutAction_idInput", {
  isAbstract: true
})
export class NotificationUpsertWithWhereUniqueWithoutAction_idInput {
  @TypeGraphQL.Field(_type => NotificationWhereUniqueInput, {
    nullable: false
  })
  where!: NotificationWhereUniqueInput;

  @TypeGraphQL.Field(_type => NotificationUpdateWithoutAction_idInput, {
    nullable: false
  })
  update!: NotificationUpdateWithoutAction_idInput;

  @TypeGraphQL.Field(_type => NotificationCreateWithoutAction_idInput, {
    nullable: false
  })
  create!: NotificationCreateWithoutAction_idInput;
}
