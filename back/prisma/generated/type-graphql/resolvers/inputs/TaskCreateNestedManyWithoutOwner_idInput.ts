import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TaskCreateManyOwner_idInputEnvelope } from "../inputs/TaskCreateManyOwner_idInputEnvelope";
import { TaskCreateOrConnectWithoutOwner_idInput } from "../inputs/TaskCreateOrConnectWithoutOwner_idInput";
import { TaskCreateWithoutOwner_idInput } from "../inputs/TaskCreateWithoutOwner_idInput";
import { TaskWhereUniqueInput } from "../inputs/TaskWhereUniqueInput";

@TypeGraphQL.InputType("TaskCreateNestedManyWithoutOwner_idInput", {
  isAbstract: true
})
export class TaskCreateNestedManyWithoutOwner_idInput {
  @TypeGraphQL.Field(_type => [TaskCreateWithoutOwner_idInput], {
    nullable: true
  })
  create?: TaskCreateWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [TaskCreateOrConnectWithoutOwner_idInput], {
    nullable: true
  })
  connectOrCreate?: TaskCreateOrConnectWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => TaskCreateManyOwner_idInputEnvelope, {
    nullable: true
  })
  createMany?: TaskCreateManyOwner_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [TaskWhereUniqueInput], {
    nullable: true
  })
  connect?: TaskWhereUniqueInput[] | undefined;
}
