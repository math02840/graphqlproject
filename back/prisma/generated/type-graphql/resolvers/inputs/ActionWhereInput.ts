import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { DateTimeNullableFilter } from "../inputs/DateTimeNullableFilter";
import { EnumActionGroupFilter } from "../inputs/EnumActionGroupFilter";
import { EnumActionNameFilter } from "../inputs/EnumActionNameFilter";
import { EnumTargetTypeFilter } from "../inputs/EnumTargetTypeFilter";
import { NotificationListRelationFilter } from "../inputs/NotificationListRelationFilter";
import { StringFilter } from "../inputs/StringFilter";
import { StringNullableFilter } from "../inputs/StringNullableFilter";
import { TaskRelationFilter } from "../inputs/TaskRelationFilter";

@TypeGraphQL.InputType("ActionWhereInput", {
  isAbstract: true
})
export class ActionWhereInput {
  @TypeGraphQL.Field(_type => [ActionWhereInput], {
    nullable: true
  })
  AND?: ActionWhereInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionWhereInput], {
    nullable: true
  })
  OR?: ActionWhereInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionWhereInput], {
    nullable: true
  })
  NOT?: ActionWhereInput[] | undefined;

  @TypeGraphQL.Field(_type => StringFilter, {
    nullable: true
  })
  id?: StringFilter | undefined;

  @TypeGraphQL.Field(_type => EnumActionGroupFilter, {
    nullable: true
  })
  group?: EnumActionGroupFilter | undefined;

  @TypeGraphQL.Field(_type => EnumActionNameFilter, {
    nullable: true
  })
  name?: EnumActionNameFilter | undefined;

  @TypeGraphQL.Field(_type => DateTimeNullableFilter, {
    nullable: true
  })
  created_at?: DateTimeNullableFilter | undefined;

  @TypeGraphQL.Field(_type => TaskRelationFilter, {
    nullable: true
  })
  target_id?: TaskRelationFilter | undefined;

  @TypeGraphQL.Field(_type => EnumTargetTypeFilter, {
    nullable: true
  })
  target_type?: EnumTargetTypeFilter | undefined;

  @TypeGraphQL.Field(_type => StringNullableFilter, {
    nullable: true
  })
  taskId?: StringNullableFilter | undefined;

  @TypeGraphQL.Field(_type => NotificationListRelationFilter, {
    nullable: true
  })
  notifications?: NotificationListRelationFilter | undefined;
}
