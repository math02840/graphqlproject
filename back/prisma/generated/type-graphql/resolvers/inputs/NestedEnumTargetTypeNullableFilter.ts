import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TargetType } from "../../enums/TargetType";

@TypeGraphQL.InputType("NestedEnumTargetTypeNullableFilter", {
  isAbstract: true
})
export class NestedEnumTargetTypeNullableFilter {
  @TypeGraphQL.Field(_type => TargetType, {
    nullable: true
  })
  equals?: "FEATURE" | "BUG" | undefined;

  @TypeGraphQL.Field(_type => [TargetType], {
    nullable: true
  })
  in?: Array<"FEATURE" | "BUG"> | undefined;

  @TypeGraphQL.Field(_type => [TargetType], {
    nullable: true
  })
  notIn?: Array<"FEATURE" | "BUG"> | undefined;

  @TypeGraphQL.Field(_type => NestedEnumTargetTypeNullableFilter, {
    nullable: true
  })
  not?: NestedEnumTargetTypeNullableFilter | undefined;
}
