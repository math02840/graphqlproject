import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TaskCreateNestedOneWithoutCommentInput } from "../inputs/TaskCreateNestedOneWithoutCommentInput";
import { TargetType } from "../../enums/TargetType";

@TypeGraphQL.InputType("CommentCreateWithoutOwner_idInput", {
  isAbstract: true
})
export class CommentCreateWithoutOwner_idInput {
  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  id?: string | undefined;

  @TypeGraphQL.Field(_type => TaskCreateNestedOneWithoutCommentInput, {
    nullable: true
  })
  target_id?: TaskCreateNestedOneWithoutCommentInput | undefined;

  @TypeGraphQL.Field(_type => TargetType, {
    nullable: true
  })
  target_type?: "FEATURE" | "BUG" | undefined;

  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  parent_id?: string | undefined;

  @TypeGraphQL.Field(_type => String, {
    nullable: false
  })
  content!: string;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  created_at?: Date | undefined;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  updated_at?: Date | undefined;
}
