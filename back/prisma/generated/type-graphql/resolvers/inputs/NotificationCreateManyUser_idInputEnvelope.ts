import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NotificationCreateManyUser_idInput } from "../inputs/NotificationCreateManyUser_idInput";

@TypeGraphQL.InputType("NotificationCreateManyUser_idInputEnvelope", {
  isAbstract: true
})
export class NotificationCreateManyUser_idInputEnvelope {
  @TypeGraphQL.Field(_type => [NotificationCreateManyUser_idInput], {
    nullable: false
  })
  data!: NotificationCreateManyUser_idInput[];

  @TypeGraphQL.Field(_type => Boolean, {
    nullable: true
  })
  skipDuplicates?: boolean | undefined;
}
