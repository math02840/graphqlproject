import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TaskCreateNestedOneWithoutActionInput } from "../inputs/TaskCreateNestedOneWithoutActionInput";
import { ActionGroup } from "../../enums/ActionGroup";
import { ActionName } from "../../enums/ActionName";
import { TargetType } from "../../enums/TargetType";

@TypeGraphQL.InputType("ActionCreateWithoutNotificationsInput", {
  isAbstract: true
})
export class ActionCreateWithoutNotificationsInput {
  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  id?: string | undefined;

  @TypeGraphQL.Field(_type => ActionGroup, {
    nullable: false
  })
  group!: "TASK" | "COMMENT";

  @TypeGraphQL.Field(_type => ActionName, {
    nullable: false
  })
  name!: "CREATE" | "ADD" | "UPDATE" | "DELETE" | "COMMENT" | "ASSIGN" | "UNASSIGN";

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  created_at?: Date | undefined;

  @TypeGraphQL.Field(_type => TaskCreateNestedOneWithoutActionInput, {
    nullable: true
  })
  target_id?: TaskCreateNestedOneWithoutActionInput | undefined;

  @TypeGraphQL.Field(_type => TargetType, {
    nullable: false
  })
  target_type!: "FEATURE" | "BUG";
}
