import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TaskCreateOrConnectWithoutActionInput } from "../inputs/TaskCreateOrConnectWithoutActionInput";
import { TaskCreateWithoutActionInput } from "../inputs/TaskCreateWithoutActionInput";
import { TaskWhereUniqueInput } from "../inputs/TaskWhereUniqueInput";

@TypeGraphQL.InputType("TaskCreateNestedOneWithoutActionInput", {
  isAbstract: true
})
export class TaskCreateNestedOneWithoutActionInput {
  @TypeGraphQL.Field(_type => TaskCreateWithoutActionInput, {
    nullable: true
  })
  create?: TaskCreateWithoutActionInput | undefined;

  @TypeGraphQL.Field(_type => TaskCreateOrConnectWithoutActionInput, {
    nullable: true
  })
  connectOrCreate?: TaskCreateOrConnectWithoutActionInput | undefined;

  @TypeGraphQL.Field(_type => TaskWhereUniqueInput, {
    nullable: true
  })
  connect?: TaskWhereUniqueInput | undefined;
}
