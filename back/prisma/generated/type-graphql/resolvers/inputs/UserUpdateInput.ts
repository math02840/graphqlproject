import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { AssigneeUpdateManyWithoutUserNestedInput } from "../inputs/AssigneeUpdateManyWithoutUserNestedInput";
import { CommentUpdateManyWithoutOwner_idNestedInput } from "../inputs/CommentUpdateManyWithoutOwner_idNestedInput";
import { NotificationUpdateManyWithoutUser_idNestedInput } from "../inputs/NotificationUpdateManyWithoutUser_idNestedInput";
import { NullableBoolFieldUpdateOperationsInput } from "../inputs/NullableBoolFieldUpdateOperationsInput";
import { NullableDateTimeFieldUpdateOperationsInput } from "../inputs/NullableDateTimeFieldUpdateOperationsInput";
import { NullableStringFieldUpdateOperationsInput } from "../inputs/NullableStringFieldUpdateOperationsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";
import { TaskUpdateManyWithoutOwner_idNestedInput } from "../inputs/TaskUpdateManyWithoutOwner_idNestedInput";

@TypeGraphQL.InputType("UserUpdateInput", {
  isAbstract: true
})
export class UserUpdateInput {
  @TypeGraphQL.Field(_type => StringFieldUpdateOperationsInput, {
    nullable: true
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableStringFieldUpdateOperationsInput, {
    nullable: true
  })
  name?: NullableStringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableStringFieldUpdateOperationsInput, {
    nullable: true
  })
  password_digest?: NullableStringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableBoolFieldUpdateOperationsInput, {
    nullable: true
  })
  active?: NullableBoolFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => GraphQLScalars.JSONResolver, {
    nullable: true
  })
  preferences?: Prisma.InputJsonValue | undefined;

  @TypeGraphQL.Field(_type => NullableStringFieldUpdateOperationsInput, {
    nullable: true
  })
  email?: NullableStringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableDateTimeFieldUpdateOperationsInput, {
    nullable: true
  })
  last_sign_in_at?: NullableDateTimeFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableDateTimeFieldUpdateOperationsInput, {
    nullable: true
  })
  created_at?: NullableDateTimeFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableDateTimeFieldUpdateOperationsInput, {
    nullable: true
  })
  updated_at?: NullableDateTimeFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => TaskUpdateManyWithoutOwner_idNestedInput, {
    nullable: true
  })
  task?: TaskUpdateManyWithoutOwner_idNestedInput | undefined;

  @TypeGraphQL.Field(_type => CommentUpdateManyWithoutOwner_idNestedInput, {
    nullable: true
  })
  comment?: CommentUpdateManyWithoutOwner_idNestedInput | undefined;

  @TypeGraphQL.Field(_type => AssigneeUpdateManyWithoutUserNestedInput, {
    nullable: true
  })
  assignees?: AssigneeUpdateManyWithoutUserNestedInput | undefined;

  @TypeGraphQL.Field(_type => NotificationUpdateManyWithoutUser_idNestedInput, {
    nullable: true
  })
  notification?: NotificationUpdateManyWithoutUser_idNestedInput | undefined;
}
