import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionUpdateManyWithoutTarget_idNestedInput } from "../inputs/ActionUpdateManyWithoutTarget_idNestedInput";
import { AssigneeUpdateManyWithoutTask_idNestedInput } from "../inputs/AssigneeUpdateManyWithoutTask_idNestedInput";
import { EnumTaskStateFieldUpdateOperationsInput } from "../inputs/EnumTaskStateFieldUpdateOperationsInput";
import { NullableDateTimeFieldUpdateOperationsInput } from "../inputs/NullableDateTimeFieldUpdateOperationsInput";
import { NullableStringFieldUpdateOperationsInput } from "../inputs/NullableStringFieldUpdateOperationsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";
import { UserUpdateOneWithoutTaskNestedInput } from "../inputs/UserUpdateOneWithoutTaskNestedInput";

@TypeGraphQL.InputType("TaskUpdateWithoutCommentInput", {
  isAbstract: true
})
export class TaskUpdateWithoutCommentInput {
  @TypeGraphQL.Field(_type => StringFieldUpdateOperationsInput, {
    nullable: true
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableStringFieldUpdateOperationsInput, {
    nullable: true
  })
  title?: NullableStringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableStringFieldUpdateOperationsInput, {
    nullable: true
  })
  description?: NullableStringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => UserUpdateOneWithoutTaskNestedInput, {
    nullable: true
  })
  owner_id?: UserUpdateOneWithoutTaskNestedInput | undefined;

  @TypeGraphQL.Field(_type => NullableDateTimeFieldUpdateOperationsInput, {
    nullable: true
  })
  due_at?: NullableDateTimeFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableDateTimeFieldUpdateOperationsInput, {
    nullable: true
  })
  created_at?: NullableDateTimeFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableDateTimeFieldUpdateOperationsInput, {
    nullable: true
  })
  updated_at?: NullableDateTimeFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => EnumTaskStateFieldUpdateOperationsInput, {
    nullable: true
  })
  state?: EnumTaskStateFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => AssigneeUpdateManyWithoutTask_idNestedInput, {
    nullable: true
  })
  assignees?: AssigneeUpdateManyWithoutTask_idNestedInput | undefined;

  @TypeGraphQL.Field(_type => ActionUpdateManyWithoutTarget_idNestedInput, {
    nullable: true
  })
  action?: ActionUpdateManyWithoutTarget_idNestedInput | undefined;
}
