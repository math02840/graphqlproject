import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NotificationUpdateWithoutUser_idInput } from "../inputs/NotificationUpdateWithoutUser_idInput";
import { NotificationWhereUniqueInput } from "../inputs/NotificationWhereUniqueInput";

@TypeGraphQL.InputType("NotificationUpdateWithWhereUniqueWithoutUser_idInput", {
  isAbstract: true
})
export class NotificationUpdateWithWhereUniqueWithoutUser_idInput {
  @TypeGraphQL.Field(_type => NotificationWhereUniqueInput, {
    nullable: false
  })
  where!: NotificationWhereUniqueInput;

  @TypeGraphQL.Field(_type => NotificationUpdateWithoutUser_idInput, {
    nullable: false
  })
  data!: NotificationUpdateWithoutUser_idInput;
}
