import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { AssigneeCreateManyTask_idInputEnvelope } from "../inputs/AssigneeCreateManyTask_idInputEnvelope";
import { AssigneeCreateOrConnectWithoutTask_idInput } from "../inputs/AssigneeCreateOrConnectWithoutTask_idInput";
import { AssigneeCreateWithoutTask_idInput } from "../inputs/AssigneeCreateWithoutTask_idInput";
import { AssigneeScalarWhereInput } from "../inputs/AssigneeScalarWhereInput";
import { AssigneeUpdateManyWithWhereWithoutTask_idInput } from "../inputs/AssigneeUpdateManyWithWhereWithoutTask_idInput";
import { AssigneeUpdateWithWhereUniqueWithoutTask_idInput } from "../inputs/AssigneeUpdateWithWhereUniqueWithoutTask_idInput";
import { AssigneeUpsertWithWhereUniqueWithoutTask_idInput } from "../inputs/AssigneeUpsertWithWhereUniqueWithoutTask_idInput";
import { AssigneeWhereUniqueInput } from "../inputs/AssigneeWhereUniqueInput";

@TypeGraphQL.InputType("AssigneeUpdateManyWithoutTask_idNestedInput", {
  isAbstract: true
})
export class AssigneeUpdateManyWithoutTask_idNestedInput {
  @TypeGraphQL.Field(_type => [AssigneeCreateWithoutTask_idInput], {
    nullable: true
  })
  create?: AssigneeCreateWithoutTask_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [AssigneeCreateOrConnectWithoutTask_idInput], {
    nullable: true
  })
  connectOrCreate?: AssigneeCreateOrConnectWithoutTask_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [AssigneeUpsertWithWhereUniqueWithoutTask_idInput], {
    nullable: true
  })
  upsert?: AssigneeUpsertWithWhereUniqueWithoutTask_idInput[] | undefined;

  @TypeGraphQL.Field(_type => AssigneeCreateManyTask_idInputEnvelope, {
    nullable: true
  })
  createMany?: AssigneeCreateManyTask_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [AssigneeWhereUniqueInput], {
    nullable: true
  })
  set?: AssigneeWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [AssigneeWhereUniqueInput], {
    nullable: true
  })
  disconnect?: AssigneeWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [AssigneeWhereUniqueInput], {
    nullable: true
  })
  delete?: AssigneeWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [AssigneeWhereUniqueInput], {
    nullable: true
  })
  connect?: AssigneeWhereUniqueInput[] | undefined;

  @TypeGraphQL.Field(_type => [AssigneeUpdateWithWhereUniqueWithoutTask_idInput], {
    nullable: true
  })
  update?: AssigneeUpdateWithWhereUniqueWithoutTask_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [AssigneeUpdateManyWithWhereWithoutTask_idInput], {
    nullable: true
  })
  updateMany?: AssigneeUpdateManyWithWhereWithoutTask_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [AssigneeScalarWhereInput], {
    nullable: true
  })
  deleteMany?: AssigneeScalarWhereInput[] | undefined;
}
