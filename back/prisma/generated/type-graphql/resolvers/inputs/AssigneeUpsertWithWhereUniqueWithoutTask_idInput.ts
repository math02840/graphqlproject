import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { AssigneeCreateWithoutTask_idInput } from "../inputs/AssigneeCreateWithoutTask_idInput";
import { AssigneeUpdateWithoutTask_idInput } from "../inputs/AssigneeUpdateWithoutTask_idInput";
import { AssigneeWhereUniqueInput } from "../inputs/AssigneeWhereUniqueInput";

@TypeGraphQL.InputType("AssigneeUpsertWithWhereUniqueWithoutTask_idInput", {
  isAbstract: true
})
export class AssigneeUpsertWithWhereUniqueWithoutTask_idInput {
  @TypeGraphQL.Field(_type => AssigneeWhereUniqueInput, {
    nullable: false
  })
  where!: AssigneeWhereUniqueInput;

  @TypeGraphQL.Field(_type => AssigneeUpdateWithoutTask_idInput, {
    nullable: false
  })
  update!: AssigneeUpdateWithoutTask_idInput;

  @TypeGraphQL.Field(_type => AssigneeCreateWithoutTask_idInput, {
    nullable: false
  })
  create!: AssigneeCreateWithoutTask_idInput;
}
