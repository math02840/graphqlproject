import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionUpdateWithoutTarget_idInput } from "../inputs/ActionUpdateWithoutTarget_idInput";
import { ActionWhereUniqueInput } from "../inputs/ActionWhereUniqueInput";

@TypeGraphQL.InputType("ActionUpdateWithWhereUniqueWithoutTarget_idInput", {
  isAbstract: true
})
export class ActionUpdateWithWhereUniqueWithoutTarget_idInput {
  @TypeGraphQL.Field(_type => ActionWhereUniqueInput, {
    nullable: false
  })
  where!: ActionWhereUniqueInput;

  @TypeGraphQL.Field(_type => ActionUpdateWithoutTarget_idInput, {
    nullable: false
  })
  data!: ActionUpdateWithoutTarget_idInput;
}
