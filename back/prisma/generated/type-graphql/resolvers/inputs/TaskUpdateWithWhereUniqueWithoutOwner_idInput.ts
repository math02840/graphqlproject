import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TaskUpdateWithoutOwner_idInput } from "../inputs/TaskUpdateWithoutOwner_idInput";
import { TaskWhereUniqueInput } from "../inputs/TaskWhereUniqueInput";

@TypeGraphQL.InputType("TaskUpdateWithWhereUniqueWithoutOwner_idInput", {
  isAbstract: true
})
export class TaskUpdateWithWhereUniqueWithoutOwner_idInput {
  @TypeGraphQL.Field(_type => TaskWhereUniqueInput, {
    nullable: false
  })
  where!: TaskWhereUniqueInput;

  @TypeGraphQL.Field(_type => TaskUpdateWithoutOwner_idInput, {
    nullable: false
  })
  data!: TaskUpdateWithoutOwner_idInput;
}
