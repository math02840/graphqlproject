import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { DateTimeNullableWithAggregatesFilter } from "../inputs/DateTimeNullableWithAggregatesFilter";
import { EnumActionGroupWithAggregatesFilter } from "../inputs/EnumActionGroupWithAggregatesFilter";
import { EnumActionNameWithAggregatesFilter } from "../inputs/EnumActionNameWithAggregatesFilter";
import { EnumTargetTypeWithAggregatesFilter } from "../inputs/EnumTargetTypeWithAggregatesFilter";
import { StringNullableWithAggregatesFilter } from "../inputs/StringNullableWithAggregatesFilter";
import { StringWithAggregatesFilter } from "../inputs/StringWithAggregatesFilter";

@TypeGraphQL.InputType("ActionScalarWhereWithAggregatesInput", {
  isAbstract: true
})
export class ActionScalarWhereWithAggregatesInput {
  @TypeGraphQL.Field(_type => [ActionScalarWhereWithAggregatesInput], {
    nullable: true
  })
  AND?: ActionScalarWhereWithAggregatesInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionScalarWhereWithAggregatesInput], {
    nullable: true
  })
  OR?: ActionScalarWhereWithAggregatesInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionScalarWhereWithAggregatesInput], {
    nullable: true
  })
  NOT?: ActionScalarWhereWithAggregatesInput[] | undefined;

  @TypeGraphQL.Field(_type => StringWithAggregatesFilter, {
    nullable: true
  })
  id?: StringWithAggregatesFilter | undefined;

  @TypeGraphQL.Field(_type => EnumActionGroupWithAggregatesFilter, {
    nullable: true
  })
  group?: EnumActionGroupWithAggregatesFilter | undefined;

  @TypeGraphQL.Field(_type => EnumActionNameWithAggregatesFilter, {
    nullable: true
  })
  name?: EnumActionNameWithAggregatesFilter | undefined;

  @TypeGraphQL.Field(_type => DateTimeNullableWithAggregatesFilter, {
    nullable: true
  })
  created_at?: DateTimeNullableWithAggregatesFilter | undefined;

  @TypeGraphQL.Field(_type => EnumTargetTypeWithAggregatesFilter, {
    nullable: true
  })
  target_type?: EnumTargetTypeWithAggregatesFilter | undefined;

  @TypeGraphQL.Field(_type => StringNullableWithAggregatesFilter, {
    nullable: true
  })
  taskId?: StringNullableWithAggregatesFilter | undefined;
}
