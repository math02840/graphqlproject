import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NotificationCreateWithoutAction_idInput } from "../inputs/NotificationCreateWithoutAction_idInput";
import { NotificationWhereUniqueInput } from "../inputs/NotificationWhereUniqueInput";

@TypeGraphQL.InputType("NotificationCreateOrConnectWithoutAction_idInput", {
  isAbstract: true
})
export class NotificationCreateOrConnectWithoutAction_idInput {
  @TypeGraphQL.Field(_type => NotificationWhereUniqueInput, {
    nullable: false
  })
  where!: NotificationWhereUniqueInput;

  @TypeGraphQL.Field(_type => NotificationCreateWithoutAction_idInput, {
    nullable: false
  })
  create!: NotificationCreateWithoutAction_idInput;
}
