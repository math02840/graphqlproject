import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { CommentCreateManyTarget_idInputEnvelope } from "../inputs/CommentCreateManyTarget_idInputEnvelope";
import { CommentCreateOrConnectWithoutTarget_idInput } from "../inputs/CommentCreateOrConnectWithoutTarget_idInput";
import { CommentCreateWithoutTarget_idInput } from "../inputs/CommentCreateWithoutTarget_idInput";
import { CommentWhereUniqueInput } from "../inputs/CommentWhereUniqueInput";

@TypeGraphQL.InputType("CommentCreateNestedManyWithoutTarget_idInput", {
  isAbstract: true
})
export class CommentCreateNestedManyWithoutTarget_idInput {
  @TypeGraphQL.Field(_type => [CommentCreateWithoutTarget_idInput], {
    nullable: true
  })
  create?: CommentCreateWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentCreateOrConnectWithoutTarget_idInput], {
    nullable: true
  })
  connectOrCreate?: CommentCreateOrConnectWithoutTarget_idInput[] | undefined;

  @TypeGraphQL.Field(_type => CommentCreateManyTarget_idInputEnvelope, {
    nullable: true
  })
  createMany?: CommentCreateManyTarget_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [CommentWhereUniqueInput], {
    nullable: true
  })
  connect?: CommentWhereUniqueInput[] | undefined;
}
