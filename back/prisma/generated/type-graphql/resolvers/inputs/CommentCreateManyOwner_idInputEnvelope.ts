import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { CommentCreateManyOwner_idInput } from "../inputs/CommentCreateManyOwner_idInput";

@TypeGraphQL.InputType("CommentCreateManyOwner_idInputEnvelope", {
  isAbstract: true
})
export class CommentCreateManyOwner_idInputEnvelope {
  @TypeGraphQL.Field(_type => [CommentCreateManyOwner_idInput], {
    nullable: false
  })
  data!: CommentCreateManyOwner_idInput[];

  @TypeGraphQL.Field(_type => Boolean, {
    nullable: true
  })
  skipDuplicates?: boolean | undefined;
}
