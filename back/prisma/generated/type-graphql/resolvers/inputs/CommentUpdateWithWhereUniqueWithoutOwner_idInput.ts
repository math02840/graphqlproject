import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { CommentUpdateWithoutOwner_idInput } from "../inputs/CommentUpdateWithoutOwner_idInput";
import { CommentWhereUniqueInput } from "../inputs/CommentWhereUniqueInput";

@TypeGraphQL.InputType("CommentUpdateWithWhereUniqueWithoutOwner_idInput", {
  isAbstract: true
})
export class CommentUpdateWithWhereUniqueWithoutOwner_idInput {
  @TypeGraphQL.Field(_type => CommentWhereUniqueInput, {
    nullable: false
  })
  where!: CommentWhereUniqueInput;

  @TypeGraphQL.Field(_type => CommentUpdateWithoutOwner_idInput, {
    nullable: false
  })
  data!: CommentUpdateWithoutOwner_idInput;
}
