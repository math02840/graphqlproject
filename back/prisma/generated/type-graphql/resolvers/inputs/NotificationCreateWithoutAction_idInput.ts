import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { UserCreateNestedOneWithoutNotificationInput } from "../inputs/UserCreateNestedOneWithoutNotificationInput";

@TypeGraphQL.InputType("NotificationCreateWithoutAction_idInput", {
  isAbstract: true
})
export class NotificationCreateWithoutAction_idInput {
  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  id?: string | undefined;

  @TypeGraphQL.Field(_type => UserCreateNestedOneWithoutNotificationInput, {
    nullable: true
  })
  user_id?: UserCreateNestedOneWithoutNotificationInput | undefined;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  readed_at?: Date | undefined;
}
