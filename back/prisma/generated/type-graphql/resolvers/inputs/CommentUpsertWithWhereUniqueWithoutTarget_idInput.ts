import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { CommentCreateWithoutTarget_idInput } from "../inputs/CommentCreateWithoutTarget_idInput";
import { CommentUpdateWithoutTarget_idInput } from "../inputs/CommentUpdateWithoutTarget_idInput";
import { CommentWhereUniqueInput } from "../inputs/CommentWhereUniqueInput";

@TypeGraphQL.InputType("CommentUpsertWithWhereUniqueWithoutTarget_idInput", {
  isAbstract: true
})
export class CommentUpsertWithWhereUniqueWithoutTarget_idInput {
  @TypeGraphQL.Field(_type => CommentWhereUniqueInput, {
    nullable: false
  })
  where!: CommentWhereUniqueInput;

  @TypeGraphQL.Field(_type => CommentUpdateWithoutTarget_idInput, {
    nullable: false
  })
  update!: CommentUpdateWithoutTarget_idInput;

  @TypeGraphQL.Field(_type => CommentCreateWithoutTarget_idInput, {
    nullable: false
  })
  create!: CommentCreateWithoutTarget_idInput;
}
