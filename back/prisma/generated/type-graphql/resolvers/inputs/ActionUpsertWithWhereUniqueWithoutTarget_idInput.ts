import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionCreateWithoutTarget_idInput } from "../inputs/ActionCreateWithoutTarget_idInput";
import { ActionUpdateWithoutTarget_idInput } from "../inputs/ActionUpdateWithoutTarget_idInput";
import { ActionWhereUniqueInput } from "../inputs/ActionWhereUniqueInput";

@TypeGraphQL.InputType("ActionUpsertWithWhereUniqueWithoutTarget_idInput", {
  isAbstract: true
})
export class ActionUpsertWithWhereUniqueWithoutTarget_idInput {
  @TypeGraphQL.Field(_type => ActionWhereUniqueInput, {
    nullable: false
  })
  where!: ActionWhereUniqueInput;

  @TypeGraphQL.Field(_type => ActionUpdateWithoutTarget_idInput, {
    nullable: false
  })
  update!: ActionUpdateWithoutTarget_idInput;

  @TypeGraphQL.Field(_type => ActionCreateWithoutTarget_idInput, {
    nullable: false
  })
  create!: ActionCreateWithoutTarget_idInput;
}
