import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NotificationCreateWithoutUser_idInput } from "../inputs/NotificationCreateWithoutUser_idInput";
import { NotificationUpdateWithoutUser_idInput } from "../inputs/NotificationUpdateWithoutUser_idInput";
import { NotificationWhereUniqueInput } from "../inputs/NotificationWhereUniqueInput";

@TypeGraphQL.InputType("NotificationUpsertWithWhereUniqueWithoutUser_idInput", {
  isAbstract: true
})
export class NotificationUpsertWithWhereUniqueWithoutUser_idInput {
  @TypeGraphQL.Field(_type => NotificationWhereUniqueInput, {
    nullable: false
  })
  where!: NotificationWhereUniqueInput;

  @TypeGraphQL.Field(_type => NotificationUpdateWithoutUser_idInput, {
    nullable: false
  })
  update!: NotificationUpdateWithoutUser_idInput;

  @TypeGraphQL.Field(_type => NotificationCreateWithoutUser_idInput, {
    nullable: false
  })
  create!: NotificationCreateWithoutUser_idInput;
}
