import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { CommentCreateManyTarget_idInput } from "../inputs/CommentCreateManyTarget_idInput";

@TypeGraphQL.InputType("CommentCreateManyTarget_idInputEnvelope", {
  isAbstract: true
})
export class CommentCreateManyTarget_idInputEnvelope {
  @TypeGraphQL.Field(_type => [CommentCreateManyTarget_idInput], {
    nullable: false
  })
  data!: CommentCreateManyTarget_idInput[];

  @TypeGraphQL.Field(_type => Boolean, {
    nullable: true
  })
  skipDuplicates?: boolean | undefined;
}
