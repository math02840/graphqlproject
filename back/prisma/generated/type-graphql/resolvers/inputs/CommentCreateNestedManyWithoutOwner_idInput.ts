import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { CommentCreateManyOwner_idInputEnvelope } from "../inputs/CommentCreateManyOwner_idInputEnvelope";
import { CommentCreateOrConnectWithoutOwner_idInput } from "../inputs/CommentCreateOrConnectWithoutOwner_idInput";
import { CommentCreateWithoutOwner_idInput } from "../inputs/CommentCreateWithoutOwner_idInput";
import { CommentWhereUniqueInput } from "../inputs/CommentWhereUniqueInput";

@TypeGraphQL.InputType("CommentCreateNestedManyWithoutOwner_idInput", {
  isAbstract: true
})
export class CommentCreateNestedManyWithoutOwner_idInput {
  @TypeGraphQL.Field(_type => [CommentCreateWithoutOwner_idInput], {
    nullable: true
  })
  create?: CommentCreateWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [CommentCreateOrConnectWithoutOwner_idInput], {
    nullable: true
  })
  connectOrCreate?: CommentCreateOrConnectWithoutOwner_idInput[] | undefined;

  @TypeGraphQL.Field(_type => CommentCreateManyOwner_idInputEnvelope, {
    nullable: true
  })
  createMany?: CommentCreateManyOwner_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [CommentWhereUniqueInput], {
    nullable: true
  })
  connect?: CommentWhereUniqueInput[] | undefined;
}
