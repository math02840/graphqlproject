import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { EnumActionGroupFieldUpdateOperationsInput } from "../inputs/EnumActionGroupFieldUpdateOperationsInput";
import { EnumActionNameFieldUpdateOperationsInput } from "../inputs/EnumActionNameFieldUpdateOperationsInput";
import { EnumTargetTypeFieldUpdateOperationsInput } from "../inputs/EnumTargetTypeFieldUpdateOperationsInput";
import { NullableDateTimeFieldUpdateOperationsInput } from "../inputs/NullableDateTimeFieldUpdateOperationsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";
import { TaskUpdateOneWithoutActionNestedInput } from "../inputs/TaskUpdateOneWithoutActionNestedInput";

@TypeGraphQL.InputType("ActionUpdateWithoutNotificationsInput", {
  isAbstract: true
})
export class ActionUpdateWithoutNotificationsInput {
  @TypeGraphQL.Field(_type => StringFieldUpdateOperationsInput, {
    nullable: true
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => EnumActionGroupFieldUpdateOperationsInput, {
    nullable: true
  })
  group?: EnumActionGroupFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => EnumActionNameFieldUpdateOperationsInput, {
    nullable: true
  })
  name?: EnumActionNameFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => NullableDateTimeFieldUpdateOperationsInput, {
    nullable: true
  })
  created_at?: NullableDateTimeFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => TaskUpdateOneWithoutActionNestedInput, {
    nullable: true
  })
  target_id?: TaskUpdateOneWithoutActionNestedInput | undefined;

  @TypeGraphQL.Field(_type => EnumTargetTypeFieldUpdateOperationsInput, {
    nullable: true
  })
  target_type?: EnumTargetTypeFieldUpdateOperationsInput | undefined;
}
