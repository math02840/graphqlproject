import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionCreateNestedOneWithoutNotificationsInput } from "../inputs/ActionCreateNestedOneWithoutNotificationsInput";

@TypeGraphQL.InputType("NotificationCreateWithoutUser_idInput", {
  isAbstract: true
})
export class NotificationCreateWithoutUser_idInput {
  @TypeGraphQL.Field(_type => String, {
    nullable: true
  })
  id?: string | undefined;

  @TypeGraphQL.Field(_type => ActionCreateNestedOneWithoutNotificationsInput, {
    nullable: true
  })
  action_id?: ActionCreateNestedOneWithoutNotificationsInput | undefined;

  @TypeGraphQL.Field(_type => Date, {
    nullable: true
  })
  readed_at?: Date | undefined;
}
