import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { AssigneeUpdateWithoutTask_idInput } from "../inputs/AssigneeUpdateWithoutTask_idInput";
import { AssigneeWhereUniqueInput } from "../inputs/AssigneeWhereUniqueInput";

@TypeGraphQL.InputType("AssigneeUpdateWithWhereUniqueWithoutTask_idInput", {
  isAbstract: true
})
export class AssigneeUpdateWithWhereUniqueWithoutTask_idInput {
  @TypeGraphQL.Field(_type => AssigneeWhereUniqueInput, {
    nullable: false
  })
  where!: AssigneeWhereUniqueInput;

  @TypeGraphQL.Field(_type => AssigneeUpdateWithoutTask_idInput, {
    nullable: false
  })
  data!: AssigneeUpdateWithoutTask_idInput;
}
