import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NotificationCreateManyUser_idInputEnvelope } from "../inputs/NotificationCreateManyUser_idInputEnvelope";
import { NotificationCreateOrConnectWithoutUser_idInput } from "../inputs/NotificationCreateOrConnectWithoutUser_idInput";
import { NotificationCreateWithoutUser_idInput } from "../inputs/NotificationCreateWithoutUser_idInput";
import { NotificationWhereUniqueInput } from "../inputs/NotificationWhereUniqueInput";

@TypeGraphQL.InputType("NotificationCreateNestedManyWithoutUser_idInput", {
  isAbstract: true
})
export class NotificationCreateNestedManyWithoutUser_idInput {
  @TypeGraphQL.Field(_type => [NotificationCreateWithoutUser_idInput], {
    nullable: true
  })
  create?: NotificationCreateWithoutUser_idInput[] | undefined;

  @TypeGraphQL.Field(_type => [NotificationCreateOrConnectWithoutUser_idInput], {
    nullable: true
  })
  connectOrCreate?: NotificationCreateOrConnectWithoutUser_idInput[] | undefined;

  @TypeGraphQL.Field(_type => NotificationCreateManyUser_idInputEnvelope, {
    nullable: true
  })
  createMany?: NotificationCreateManyUser_idInputEnvelope | undefined;

  @TypeGraphQL.Field(_type => [NotificationWhereUniqueInput], {
    nullable: true
  })
  connect?: NotificationWhereUniqueInput[] | undefined;
}
