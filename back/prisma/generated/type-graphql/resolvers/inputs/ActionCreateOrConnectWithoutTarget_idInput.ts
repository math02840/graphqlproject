import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { ActionCreateWithoutTarget_idInput } from "../inputs/ActionCreateWithoutTarget_idInput";
import { ActionWhereUniqueInput } from "../inputs/ActionWhereUniqueInput";

@TypeGraphQL.InputType("ActionCreateOrConnectWithoutTarget_idInput", {
  isAbstract: true
})
export class ActionCreateOrConnectWithoutTarget_idInput {
  @TypeGraphQL.Field(_type => ActionWhereUniqueInput, {
    nullable: false
  })
  where!: ActionWhereUniqueInput;

  @TypeGraphQL.Field(_type => ActionCreateWithoutTarget_idInput, {
    nullable: false
  })
  create!: ActionCreateWithoutTarget_idInput;
}
