import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { DateTimeNullableFilter } from "../inputs/DateTimeNullableFilter";
import { EnumActionGroupFilter } from "../inputs/EnumActionGroupFilter";
import { EnumActionNameFilter } from "../inputs/EnumActionNameFilter";
import { EnumTargetTypeFilter } from "../inputs/EnumTargetTypeFilter";
import { StringFilter } from "../inputs/StringFilter";
import { StringNullableFilter } from "../inputs/StringNullableFilter";

@TypeGraphQL.InputType("ActionScalarWhereInput", {
  isAbstract: true
})
export class ActionScalarWhereInput {
  @TypeGraphQL.Field(_type => [ActionScalarWhereInput], {
    nullable: true
  })
  AND?: ActionScalarWhereInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionScalarWhereInput], {
    nullable: true
  })
  OR?: ActionScalarWhereInput[] | undefined;

  @TypeGraphQL.Field(_type => [ActionScalarWhereInput], {
    nullable: true
  })
  NOT?: ActionScalarWhereInput[] | undefined;

  @TypeGraphQL.Field(_type => StringFilter, {
    nullable: true
  })
  id?: StringFilter | undefined;

  @TypeGraphQL.Field(_type => EnumActionGroupFilter, {
    nullable: true
  })
  group?: EnumActionGroupFilter | undefined;

  @TypeGraphQL.Field(_type => EnumActionNameFilter, {
    nullable: true
  })
  name?: EnumActionNameFilter | undefined;

  @TypeGraphQL.Field(_type => DateTimeNullableFilter, {
    nullable: true
  })
  created_at?: DateTimeNullableFilter | undefined;

  @TypeGraphQL.Field(_type => EnumTargetTypeFilter, {
    nullable: true
  })
  target_type?: EnumTargetTypeFilter | undefined;

  @TypeGraphQL.Field(_type => StringNullableFilter, {
    nullable: true
  })
  taskId?: StringNullableFilter | undefined;
}
