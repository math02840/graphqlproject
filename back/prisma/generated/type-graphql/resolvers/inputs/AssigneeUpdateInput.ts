import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { NullableDateTimeFieldUpdateOperationsInput } from "../inputs/NullableDateTimeFieldUpdateOperationsInput";
import { StringFieldUpdateOperationsInput } from "../inputs/StringFieldUpdateOperationsInput";
import { TaskUpdateOneWithoutAssigneesNestedInput } from "../inputs/TaskUpdateOneWithoutAssigneesNestedInput";
import { UserUpdateOneWithoutAssigneesNestedInput } from "../inputs/UserUpdateOneWithoutAssigneesNestedInput";

@TypeGraphQL.InputType("AssigneeUpdateInput", {
  isAbstract: true
})
export class AssigneeUpdateInput {
  @TypeGraphQL.Field(_type => StringFieldUpdateOperationsInput, {
    nullable: true
  })
  id?: StringFieldUpdateOperationsInput | undefined;

  @TypeGraphQL.Field(_type => TaskUpdateOneWithoutAssigneesNestedInput, {
    nullable: true
  })
  task_id?: TaskUpdateOneWithoutAssigneesNestedInput | undefined;

  @TypeGraphQL.Field(_type => UserUpdateOneWithoutAssigneesNestedInput, {
    nullable: true
  })
  user?: UserUpdateOneWithoutAssigneesNestedInput | undefined;

  @TypeGraphQL.Field(_type => NullableDateTimeFieldUpdateOperationsInput, {
    nullable: true
  })
  created_at?: NullableDateTimeFieldUpdateOperationsInput | undefined;
}
