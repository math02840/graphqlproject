import * as TypeGraphQL from "type-graphql";
import * as GraphQLScalars from "graphql-scalars";
import { Prisma } from "@prisma/client";
import { DecimalJSScalar } from "../../scalars";
import { TaskCreateWithoutOwner_idInput } from "../inputs/TaskCreateWithoutOwner_idInput";
import { TaskWhereUniqueInput } from "../inputs/TaskWhereUniqueInput";

@TypeGraphQL.InputType("TaskCreateOrConnectWithoutOwner_idInput", {
  isAbstract: true
})
export class TaskCreateOrConnectWithoutOwner_idInput {
  @TypeGraphQL.Field(_type => TaskWhereUniqueInput, {
    nullable: false
  })
  where!: TaskWhereUniqueInput;

  @TypeGraphQL.Field(_type => TaskCreateWithoutOwner_idInput, {
    nullable: false
  })
  create!: TaskCreateWithoutOwner_idInput;
}
