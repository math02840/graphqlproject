import { Arg, Mutation, Query, Resolver } from 'type-graphql';
import { Task } from '../../prisma/generated/type-graphql';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

@Resolver()
export class DeleteTaskResolver {
    @Mutation(() => Task)
    async deleteTask(
        @Arg('id') id: string,
    ): Promise<boolean> {

        await prisma.assignee.deleteMany({
            where: {
                taskId: id,
            },
        });

        await prisma.comment.deleteMany({
            where: {
                taskId: id
            }
        })

        await prisma.task.delete({
            where: {
                id,
            },
        });

        return true;
    }
}
