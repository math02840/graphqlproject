import { Arg, Mutation, Query, Resolver } from 'type-graphql';
import { Task } from '../../prisma/generated/type-graphql';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

@Resolver()
export class DeleteCommentResolver {
    @Mutation(() => Task)
    async deleteComment(
        @Arg('id') id: string,
        @Arg('email') email: string,
    ): Promise<boolean> {

        const user = await prisma.user.findUnique({
            where: {
                email,
            },
        });

        const comment = await prisma.comment.findUnique({
            where: {
                id,
            },
        });

        if (comment.userId !== user.id) {
            throw new Error('This comment does not belong to this user');
        }

        await prisma.comment.deleteMany({
            where: {
                parent_id: id,
            },
        });

        await prisma.comment.delete({
            where: {
                id,
            },
        });

        return true;
    }
}
