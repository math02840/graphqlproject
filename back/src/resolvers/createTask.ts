import { Arg, Mutation, Query, Resolver } from 'type-graphql';
import { Task } from '../../prisma/generated/type-graphql';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

@Resolver()
export class CreateTaskResolver {
  @Mutation(() => Task)
  async createTask(
    @Arg('title') title: string,
    @Arg('description') description: string,
    @Arg('email') email: string,
  ): Promise<Task> {

    const user = await prisma.user.findUnique({
      where: {
        email,
      },
    });

    const task = await prisma.task.create({
      data: {
        title: title,
        description: description,
        state: 'TODO',
        userId: user.id,
        created_at: new Date().toISOString(),
      },
    });

    return task;
  }
}
