import { Arg, Mutation, Resolver } from 'type-graphql';
import { User } from '../../prisma/generated/type-graphql';
import * as bcrypt from 'bcrypt';
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

@Resolver()
export class LoginResolver {
    @Mutation(() => User)
    async login(
        @Arg('email') email: string,
        @Arg('password') password: string,
    ): Promise<User> {
        const user = await prisma.user.findUnique({ where: { email } })
    
        const valid = await bcrypt.compare(password, user.password_digest);

        if (!valid || !user) {
            throw new Error('Invalid credentials or no user found');
        }
        
        return user;
    }
}