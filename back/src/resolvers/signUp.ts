import { Arg, Mutation, Resolver } from 'type-graphql';
import { User } from '../../prisma/generated/type-graphql';
import { PrismaClient } from '@prisma/client';
import * as bcrypt from 'bcrypt';

const prisma = new PrismaClient();

@Resolver()
export class SignUpResolver {
    @Mutation(() => User)
    async signUp(
        @Arg('email') email: string,
        @Arg('password_digest') password_digest: string,
        @Arg('name') name: string,
    ): Promise<User> {

        const isAlreadySignUp = await prisma.user.findUnique({
            where: {
                email,
            },
        });

        if (isAlreadySignUp) {
            throw new Error('User already exists');
        }

        const passwordHash = await bcrypt.hash(password_digest, 10)
        const user = await prisma.user.create({
            data: {
                email,
                password_digest: passwordHash,
                name,
                active: false,
                preferences: {},
            },
        })

        return user;
    }
}