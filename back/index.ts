import "reflect-metadata";
import * as TypeGraphQL from "type-graphql";
import { ApolloServer } from "apollo-server";
import { PrismaClient } from "@prisma/client";
import { WebSocketServer } from 'ws';

import { useServer } from 'graphql-ws/lib/use/ws';

import { RedisPubSub } from 'graphql-redis-subscriptions';
import { resolvers as generatedResolvers } from "./prisma/generated/type-graphql";
import { SignUpResolver as signUpResolver } from './src/resolvers/signUp';
import { LoginResolver as loginResolver } from './src/resolvers/login';
import { CreateTaskResolver as createTaskResolver } from './src/resolvers/createTask';
import { DeleteTaskResolver as deleteTaskResolver } from "./src/resolvers/deleteTask";
import { DeleteCommentResolver as deleteCommentResolver } from "./src/resolvers/deleteComment";
import { CommentTaskResolver as commentTaskResolver } from "./src/resolvers/commentTask";
import { AssigneeTaskToUserResolver as assigneeTaskToUserResolver } from "./src/resolvers/assigneeTask";

interface Context {
  prisma: PrismaClient;
}

const { getUserId } = require('./src/utils');

const pubSub = new RedisPubSub({
  messageEventName: 'messageBuffer',
  pmessageEventName: 'pmessageBuffer',
});

const resolvers = [
  ...generatedResolvers,
  signUpResolver,
  loginResolver,
  createTaskResolver,
  deleteTaskResolver,
  deleteCommentResolver,
  commentTaskResolver,
  assigneeTaskToUserResolver,
] as TypeGraphQL.NonEmptyArray<Function>;

async function main() {
  const schema = await TypeGraphQL.buildSchema({
    resolvers: resolvers,
    pubSub,
    emitSchemaFile: "./generated-schema.graphql",
    validate: false,
  });

  const prisma = new PrismaClient();
  const server = new ApolloServer({
    schema,
    context: ({ req }): Context => {
      return {
        ...req,
        prisma,
        userId:
          req && req.headers.authorization ? getUserId(req) : null
      }
    },
    cors: {
      origin: ['http://localhost:8000', 'https://studio.apollographql.com'],
      credentials: true,
    },
    introspection: true,
  });
  const wsServer = new WebSocketServer({
    server: server.httpServer,
    path: '/subscriptions',
  });
  const serverCleanup = useServer({ schema }, wsServer);

  const { port } = await server.listen(4000);
  console.log(`GraphQL is listening on ${port}!`);
}

main().catch(console.error);
