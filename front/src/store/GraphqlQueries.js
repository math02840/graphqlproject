import { ApolloClient, InMemoryCache, gql } from '@apollo/client'
import User from 'store/User'

export const getClient = () => {
  return new ApolloClient({
    uri: process.env.GATSBY_API_URL,
    cache: new InMemoryCache({
      possibleTypes: {},
    }),
    credentials: 'include',
    headers: {
      user: User.get('account', 'id') || null,
    },
  })
}

export const LOGIN_USER_MUTATION = gql`
  mutation loginUser($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      id
      name
      email
    }
  }
`

export const CREATE_USER_MUTATION = gql`
  mutation createUser($email: String!, $password: String!, $name: String!) {
    signUp(email: $email, password_digest: $password, name: $name) {
      id
      name
      email
      password_digest
    }
  }
`

export const GET_ALL_TASKS = gql`
  query tasks {
    tasks {
      id
      title
      state
      due_at
      assignees {
        user {
          name
        }
      }
    }
  }
`

export const GET_ONE_TASK = gql`
  query task($where: TaskWhereUniqueInput!) {
    task(where: $where) {
      id
      title
      description
      state
      assignees {
        user {
          name
        }
      }
      comments {
        id
        content
        owner {
          id
          name
        }
      }
    }
  }
`

export const CREATE_TASK_MUTATION = gql`
  mutation createTask($title: String!, $description: String!, $email: String!) {
    createTask(title: $title, description: $description, email: $email) {
      title
      description
    }
  }
`

export const UPDATE_TASK_MUTATION = gql`
  mutation updateOneTask(
    $data: TaskUpdateInput!
    $where: TaskWhereUniqueInput!
  ) {
    updateOneTask(data: $data, where: $where) {
      state
    }
  }
`

export const DELETE_TASK_MUTATION = gql`
  mutation deleteTask($id: String!) {
    deleteTask(id: $id) {
      id
    }
  }
`

export const CREATE_ASSIGNEE_MUTATION = gql`
  mutation assigneeTask($taskId: String!, $userId: String!) {
    assigneeTask(taskId: $taskId, userId: $userId) {
      taskId
      userId
    }
  }
`

export const CREATE_COMMENT_MUTATION = gql`
  mutation commentTask(
    $content: String!
    $taskId: String!
    $userId: String!
  ) {
    commentTask(content: $content, taskId: $taskId, userId: $userId) {
      ownerId
      taskId
      content
    }
  }
`

export const DELETE_COMMENT_MUTATION = gql`
  mutation deleteComment($id: String!) {
    deleteComment(id: $id) {
      id
    }
  }
`
