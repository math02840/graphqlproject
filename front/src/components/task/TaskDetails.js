import React, { useState } from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import Form from 'react-bootstrap/Form'
import {
    CREATE_COMMENT,
    GET_ONE_TASK,
    UPDATE_TASK,
} from '../../store/GraphqlQueries'
import Button from 'react-bootstrap/Button'

const STATE = [
    { value: 'TODO', label: 'TODO' },
    { value: 'IN_PROGRESS', label: 'IN_PROGRESS' },
    { value: 'DONE', label: 'DONE' },
]

const TaskDetails = ({ id }) => {

    const [commentContent, setCommentContent] = useState();

    const { data } = JSON.parse(localStorage.getItem('User.current'));

    const {
        loading,
        error,
        data: taskData,
        refetch,
    } = useQuery(GET_ONE_TASK, {
        variables: { where: { id } },
    });
    const [updateOneTask] = useMutation(UPDATE_TASK);
    const [commentTask] = useMutation(CREATE_COMMENT);

    const onSubmit = (e) => {
        e.preventDefault()

        commentTask({
            variables: {
                userId: data.signIn.id,
                taskId: id,
                content: commentContent,
            },
        })
            .then(() => {
                setCommentContent('');
                refetch();
            })
            .catch((error) => {
                alert(error)
            })

        return false
    }

    const handleStateChange = (state) => {

        updateOneTask({
            variables: {
                where: {
                    id,
                },
                data: {
                    state: {
                        set: state.value,
                    },
                },
            },
        })
            .then(() => {
                alert('Task state updated')
            })
            .catch((error) => {
                alert(error)
            })

        return false
    }

    const deleteCommentHandler = (id) => {
        deleteTask({ variables: { id: id } })
            .then(() => {
                refetch();
            })
            .catch((error) => {
                alert(error)
            })
    }

    if (loading) return 'Loading...';
    if (error) return `Error! ${error.message}`;

    return (
        <div className="col">
            <h1>Task details</h1>
            <p>Title : {taskData.task.title}</p>
            <p>State :
                <Select
                    options={STATE}
                    onChange={(value) => handleStateChange(value)}
                    defaultValue={{
                        value: taskData.task.state,
                        label: taskData.task.state,
                    }}
                />
            </p>
            <p>Assignees : {taskData.task.assignees.map((user) => user.user.name)}</p>
            <p>Description : {taskData.task.description}</p>

            <h2>Comments</h2>
            {taskData.task.comments.map((comment) => (
                <div class="d-flex flex-row">
                    <div class="comment-text w-100">
                        {comment.owner.id === data.signIn.id && (
                            <Button onClick={() => deleteCommentHandler(comment.id)}>
                                Delete
                            </Button>
                        )}
                        <b>{comment.owner.name}</b>
                        <br />
                        {comment.content}
                    </div>
                </div>
            ))}

            <Form onSubmit={onSubmit}>
                <Form.Group className="mb-2">
                    <Form.Label>Comment task</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter your comment"
                        onChange={(e) => setCommentContent(e.target.value)}
                    />
                </Form.Group>
                <Button variant="success" type="submit">
                    Post comment
                </Button>
            </Form>
        </div>
    )
}
export default TaskDetails
