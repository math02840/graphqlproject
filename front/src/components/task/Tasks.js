import React from 'react'

import Table from 'react-bootstrap/Table'
import Badge from 'react-bootstrap/Badge'

import { useQuery, useMutation } from '@apollo/react-hooks'
import { GET_ALL_TASKS, DELETE_TASK_MUTATION } from '../../store/GraphqlQueries'
import User from 'store/User'
import Button from 'react-bootstrap/esm/Button'

import { navigate } from 'gatsby'

const Tasks = () => {

  const currentUser = User.get('current')?.data.login

  const { loading, error, data, refetch } = useQuery(GET_ALL_TASKS);
  const [deleteTask] = useMutation(DELETE_TASK_MUTATION)

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  const deleteTaskHandler = (id) => {
    deleteTask({ variables: { id: id } })
      .then(() => {
        refetch();
      })
      .catch((error) => {
        alert(error)
      })
  }

  return (
    currentUser ?
      <div className="col">
        <a href="/new-task" className="btn btn-success mb-3 mt-5">
          Create new task
        </a>
        <Table variant="light" striped bordered hover>
          <thead>
            <tr>
              <th>Title</th>
              <th>State</th>
              <th>Due Date</th>
              <th>Assignee</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {data.tasks.map((task) => (
              <tr key={task.id}>
                <td>{task.title}</td>
                <td>
                  {
                    task.state == 'TODO' ?
                      <Badge bg="warning">{task.state}</Badge>
                      : task.state == 'IN_PROGRESS' ?
                        <Badge bg="info">{task.state}</Badge>
                        : task.state == 'DONE' &&
                        <Badge bg="success">{task.state}</Badge>
                  }
                </td>
                <td>{task.due_at}</td>
                <td>
                  {task?.assignees.map((user) => (
                    <p>{user.user.name}</p>
                  ))}
                </td>
                <td><Button variant="success" onClick={() => navigate(`/task-details/${task.id}`)}>Edit</Button></td>
                <td><Button variant="danger" onClick={() => deleteTaskHandler(task.id)}>Delete</Button></td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
      : <div className="col">You must be logged in to see the tasks</div>
  )
}

export default Tasks
