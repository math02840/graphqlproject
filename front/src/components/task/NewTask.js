import React, { useState } from 'react'
import { useMutation } from '@apollo/react-hooks'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { navigate } from 'gatsby'
import { CREATE_TASK_MUTATION } from '../../store/GraphqlQueries'
import User from 'store/User'

const NewTask = () => {

    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');

    const currentUserEmail = User.get('current')?.data.login.email

    const [createTask] = useMutation(CREATE_TASK_MUTATION);

    const onSubmit = (e) => {

        e.preventDefault()

        createTask({
            variables: {
                title: title,
                description: description,
                email: currentUserEmail,
            },
        })
            .then(() => {
                navigate('/')
            })
            .catch((error) => {
                alert(error)
            })

        return false
    }

    return (
        <div className="col">
            <Form onSubmit={onSubmit}>
                <Form.Group className="mb-3" controlId="formBasicTitle">
                    <Form.Label>Titre</Form.Label>
                    <Form.Control
                        placeholder="Enter title"
                        onChange={(e) => setTitle(e.target.value)}
                        name="title"
                        defaultValue={title}
                    />
                    <Form.Text className="text-muted"></Form.Text>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicDescription">
                    <Form.Label>Description</Form.Label>
                    <Form.Control
                        placeholder="Enter description"
                        onChange={(e) => setDescription(e.target.value)}
                        name="description"
                        defaultValue={description}
                    />
                    <Form.Text className="text-muted"></Form.Text>
                </Form.Group>

                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </div>
    )
}
export default NewTask
