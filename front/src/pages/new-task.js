import React from 'react'
import Layout from 'components/Layout/Layout'
import Content from 'components/task/NewTask'

const Page = () => (
  <Layout id="new-task" loginRequired={true}>
    <Content />
  </Layout>
)

export default Page
export { Head } from 'components/Layout/Head'
