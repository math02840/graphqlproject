import React from 'react'
import Layout from 'components/Layout/Layout'
import Content from 'components/task/TaskDetails'

const Page = ({ params }) => {
  return (
    <Layout id="task-details" loginRequired={true}>
      <Content id={params?.id} />
    </Layout>
  )
}
export default Page
export { Head } from 'components/Layout/Head'
